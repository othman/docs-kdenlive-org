# Spanish translations for docs_kdenlive_org_effects_and_compositions___video_effects___color_image_correction___contrast.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2023.
# Eloy Cuadra <ecuadra@eloihr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___video_effects___color_image_correction___contrast\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-07-22 20:51-0300\n"
"Last-Translator: Gabriel Gazzán <gabcorreo@gmail.com>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 3.3.2\n"

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:1
msgid "Kdenlive Video Effects - Contrast"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, color and image correction, contrast "
msgstr ""
"KDE, Kdenlive, efectos, filtro, efectos de video, corrección de color e "
"imagen, interfaz de usuario, documentación, manual de usuario, editor de "
"video, código abierto, libre, aprender, fácil, contraste "

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:1
msgid "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "
msgstr "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Licencia Creative Commons SA 4.0"

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:12
#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:56
msgid "Contrast"
msgstr "Contraste"

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:23
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:25
msgid "Yes"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:27
msgid "frei0r"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:29
#, fuzzy
#| msgid "Contrast"
msgid "contrast0r"
msgstr "Contraste"

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:31
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:33
#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:35
msgid "No"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:41
#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:55
msgid "Description"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:42
#, fuzzy
#| msgid ""
#| "This filter controls the tonal range of the source which maps pixel "
#| "values below a specified value to black, and those above a specified "
#| "value to white. It is best used in conjunction with :ref:`view-"
#| "rgb_parade` and the :ref:`view-histogram`. Values below 250 decrease the "
#| "contrast between dark and light areas, values above 250 increase it."
msgid ""
"This effect/filter adjusts the contrast of a source image. It controls the "
"tonal range of the source which maps pixel values below a specified value to "
"black, and those above a specified value to white. It is best used in "
"conjunction with :ref:`view-rgb_parade` and the :ref:`view-histogram`. "
"Values below 250 decrease the contrast between dark and light areas, values "
"above 250 increase it."
msgstr ""
"Este efecto controla el rango tonal de la imagen, mapeando valores de píxel "
"por debajo de determinado umbral a negro y aquellos por sobre otro umbral a "
"blanco. Es habitual lsu uso en conjunto con los instrumentos: :ref:`view-"
"rgb_parade` y :ref:`view-histogram`. Valores menores de 250 disminuirán el "
"contraste entre las áreas negras y blancas, mientras que valores superiores "
"a 250 lo aumentarán."

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:46
msgid "Parameters"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:53
msgid "Parameter"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:54
msgid "Value"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:57
msgid "Integer"
msgstr ""

#: ../../effects_and_compositions/video_effects/color_image_correction/contrast.rst:58
#, fuzzy
#| msgid ""
#| "**Contrast** - Set the contrast. Allowed values are from 0 to 1000, "
#| "default value is 250."
msgid ""
"Set the contrast. Allowed values are from 0 to 1000, default value is 250."
msgstr ""
"**Contraste** - Define el nivel de contraste. Los valores permitidos son de "
"0 a 1000, predeterminado 250."

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using contrast effect"
#~ msgstr ""
#~ "Primeros pasos con el editor de video Kdenlive, usando el efecto Contraste"

#~ msgid "This effect/filter adjusts the contrast of a source image."
#~ msgstr "Este efecto ajusta el contraste de la imagen del clip."

#~ msgid "This effect has keyframes"
#~ msgstr "Este efecto utiliza fotogramas clave"

#~ msgid "Contrast effect"
#~ msgstr "El efecto Contraste"
