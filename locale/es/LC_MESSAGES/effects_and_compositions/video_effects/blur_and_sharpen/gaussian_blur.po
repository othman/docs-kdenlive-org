# Spanish translations for docs_kdenlive_org_effects_and_compositions___video_effects___blur_and_sharpen___gaussian_blur.po package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Automatically generated, 2023.
# Eloy Cuadra <ecuadra@eloihr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_kdenlive_org_effects_and_compositions___video_effects___blur_and_sharpen___gaussian_blur\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-11 00:36+0000\n"
"PO-Revision-Date: 2023-07-05 18:33+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.2\n"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:1
msgid "Kdenlive Video Effects - Gaussian Blur (gblur)"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, blur and sharpen, gaussian blur, gblur "
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:1
msgid "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "
msgstr "- Bernd Jordan (https://discuss.kde.org/u/berndmj) "

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:1
msgid "Creative Commons License SA 4.0"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:13
msgid "Gaussian Blur"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:24
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:26
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:34
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:36
msgid "No"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:28
msgid "avfilter"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:30
msgid "gblur"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:32
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:42
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:56
msgid "Description"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:43
msgid ""
"This effect blurs the clip by applying a Gaussian function (known from "
"statistics as an expression for the normal distribution). By default, all :"
"term:`planes<plane>` will be affected. By setting the blur effect to "
"different planes (e.g. red, green or blue) interesting artistic effects can "
"be achieved."
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:47
msgid "Parameters"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:54
msgid "Parameter"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:55
msgid "Value"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:57
msgid "Sigma"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:58
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:67
msgid "Integer"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:59
msgid ""
"Set horizontal sigma, standard deviation of Gaussian blur. Determines the "
"strength of the blur. Default is 0.5."
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:60
msgid "StepsX"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:61
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:64
msgid "Selection"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:62
msgid "Set number of steps for Gaussian approximation"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:63
msgid "Planes"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:65
msgid "Set which planes to filter. By default all planes are filtered (YUV)."
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:66
msgid "Vertical Sigma"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:68
msgid ""
"Set vertical sigma, if negative it will be same as sigma. Default is -1."
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:70
msgid "The following selection items are available:"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:72
msgid ":guilabel:`StepsX`"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:79
msgid "1 thru 6"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:80
msgid "Gradually increases the blur fineness"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:82
msgid ":guilabel:`Planes`"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:89
msgid "Alpha"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:90
msgid "Alpha channel"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:91
msgid "Y"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:92
msgid "Luminance"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:93
msgid "U"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:94
msgid "Chroma (U plane)"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:95
msgid "YU"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:96
msgid "Both Y and U planes"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:97
msgid "V"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:98
msgid "Chroma (V plane)"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:99
msgid "YV"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:100
msgid "Both Y and V planes"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:101
msgid "UV"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:102
msgid "Both U and V planes"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:103
msgid "YUV"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:104
msgid "All planes will be affected by the blur effect (default)"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:105
msgid "None"
msgstr ""

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/gaussian_blur.rst:106
msgid "No effect application"
msgstr ""
