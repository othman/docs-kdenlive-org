# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# SPDX-FileCopyrightText: 2021, 2023 Roman Paholik <wizzardsk@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-12-09 16:57+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../effects_and_compositions/transitions/alphaatop.rst:11
msgid "alphaatop transition"
msgstr "alphaatop prechod"

#: ../../effects_and_compositions/transitions/alphaatop.rst:15
msgid ""
"This is the `Frei0r alphaatop <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaatop>`_ MLT transition."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:17
msgid "The alpha ATOP operation."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr ""

#: ../../effects_and_compositions/transitions/alphaatop.rst:23
msgid "alphaatop is the transition in between."
msgstr ""

#~ msgid "Contents"
#~ msgstr "Obsah"
