# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2023 Roman Paholik <wizzardsk@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-12-10 11:36+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.3\n"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
#, fuzzy
msgid "**Status**"
msgstr "Stav"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
#, fuzzy
msgid "**Keyframes**"
msgstr "Kľúčové snímky"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
#, fuzzy
msgid "**Source filter**"
msgstr "Zdrojový filter"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
#, fuzzy
msgid "**Available**"
msgstr "Dostupný"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:1
msgid "Kdenlive Video Effects - Crop by Padding"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, transform, distort, perspective, crop by padding"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:14
msgid "Crop by Padding"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:25
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:27
msgid "Yes"
msgstr "Áno"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:29
msgid "MLT"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:31
msgid "qtcrop"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:33
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:35
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:37
msgid "No"
msgstr "Nie"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:43
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:57
msgid "Description"
msgstr "Popis"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:44
msgid ""
"This effect/filter crops the image to a rounded rectangle or circle by "
"padding the edges with a specified color."
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:48
msgid "Parameters"
msgstr "Parametre"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:55
msgid "Parameter"
msgstr "Parameter"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:56
msgid "Value"
msgstr "Hodnota"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:58
#, fuzzy
msgid "Circle"
msgstr "Kruh"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:59
#, fuzzy
msgid "Switch"
msgstr "Prepnúť"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:60
msgid ""
"If switched on, the :guilabel:`Radius` parameter creates a circular crop. "
"Otherwise a rectangle is used. Default is **off**."
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:61
msgid "Radius"
msgstr "Polomer"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:62
msgid "Float"
msgstr "Reálne číslo"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:63
msgid "Amount of circular or rectangle rounding"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:64
msgid "Padding Color"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:65
msgid "Picker"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:66
msgid "The color to be used for padding. Can be alpha."
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:70
msgid "Notes"
msgstr "Poznámky"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/crop_padding.rst:71
msgid ""
"The parameters **X**, **Y**, **W**, **H**, and **Size** can be used to move "
"and/or scale the rectangle or circle within the frame in order to crop a "
"specific portion of the image."
msgstr ""
