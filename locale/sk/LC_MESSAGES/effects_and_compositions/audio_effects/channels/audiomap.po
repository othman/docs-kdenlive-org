# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2023 Roman Paholik <wizzardsk@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-15 00:36+0000\n"
"PO-Revision-Date: 2023-12-10 11:33+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.3\n"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:0
#, fuzzy
msgid "**Keyframes**"
msgstr "Kľúčové snímky"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:0
#, fuzzy
msgid "**Available**"
msgstr "Dostupný"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:1
msgid "Kdenlive Audio Effects - Audiomap"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, audio "
"effects, audiomap, mapping, balance"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:14
msgid "Audiomap"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:24
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:26
#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:32
#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:34
msgid "No"
msgstr "Nie"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:28
msgid "avfilter"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:30
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:40
#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:52
msgid "Description"
msgstr "Popis"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:41
msgid ""
"Allows for up to six audio source channels to be mapped to up to 32 channels."
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:44
msgid "Parameters"
msgstr "Parametre"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:50
msgid "Parameter"
msgstr "Parameter"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:51
msgid "Value"
msgstr "Hodnota"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:54
msgid "**CH x source**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:55
msgid "Selection"
msgstr "Výber"

#: ../../effects_and_compositions/audio_effects/channels/audiomap.rst:56
msgid "Maps source channel CH x to the selected channel (CH 1 .. CH32)"
msgstr ""
