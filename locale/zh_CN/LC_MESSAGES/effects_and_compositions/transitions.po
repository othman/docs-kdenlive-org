msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-12-30 13:42\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___transitions.pot\n"
"X-Crowdin-File-ID: 26205\n"

#: ../../effects_and_compositions/transitions.rst:19
msgid "Transitions/Compositions"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:21
msgid ""
"In **Kdenlive** a transition is a wipe or dissolve composition between two "
"overlapping clips."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:23
msgid ""
"There are two ways of adding transitions in **Kdenlive**: **Mix clips**, aka "
"same tracks transitions, and the legacy way, which is transitions between "
"clips on different tracks."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:25
msgid ""
"Wipes are greyscale images in :file:`.pgm` (Portable GreyMap) format, and "
"during the transition the composition track will be displayed in the darkest "
"areas of the wipe image first. If the wipe is inverted, the composition "
"track will become visible in the brightest areas of the wipe image first "
"instead. You can download more wipes (by clicking the download button in the "
"wipe composition properties) or create your own and load them by clicking "
"the folder button."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:30
msgid "Mixes"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:34
msgid "Mixes are transitions between clips on the same track."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:40
msgid ""
"Kdenlive with mixed clips in the timeline. The toolbar button is circled "
"with red, and the transition properties are on the right."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:42
msgid "To use it:"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:44
msgid ""
"Ensure that there is at least half a second worth of frames at the end of "
"both clips (outside the timeline clip). Without that, Kdenlive will not "
"apply the transition, but display an error message."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:45
msgid ""
"Select either clip. If there are clips in both ends of the one selected, the "
"transition will be added nearest the playhead."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:47
msgid "Ready to add the transition:"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:49
msgid "**Keyboard:** Press the :kbd:`U` key."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:50
msgid ""
"**Mouse:** double-click at the point where the clips meet. This does not "
"require a clip to be selected."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:51
msgid ""
"**Toolbar:** Press the :guilabel:`Mix Clips` button on the timeline toolbar."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:53
msgid ""
"The transition length defaults to one second equally distributed between the "
"two clips. You can drag either end of the transition to adjust."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:55
msgid ""
"The default transition is dissolve, but you can select (click) the "
"transition and edit the wipe method and properties as desired in the effect/"
"transition stack window."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:57
msgid ""
"The composition method is set to Luma, and it does not make sense to change "
"that, since there will not really be a transition. Other methods are meant "
"for compositing two videos, not transitioning between them."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:59
msgid "Select the desired wipe from Wipe Method."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:60
msgid "The :guilabel:`Reverse` option reverses the transition."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:61
msgid ""
"The :guilabel:`Softness:` slider will affect the edges of wipe transitions."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:62
msgid "Set the desired duration."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:63
msgid ""
"Since 21.08: select the alignment of the transition, either left, centered "
"(default) or right."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:65
msgid "To delete the transition, select it and press the :kbd:`Delete` key."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:69
msgid ""
"To apply only transitions on either the video or audio track just lock the "
"track on which you don’t want a transition to be applied and add transitions "
"by one of the above ways on the other track."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:72
msgid "Compositions"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:74
msgid ""
"Compositions are transitions between clips on different tracks. This is the "
"legacy way of doing transitions in **Kdenlive**."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:80
msgid ""
"To the left the purple one-click circle to add a wipe composition. In the "
"middle the composition bar, and on the right the properties."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:82
msgid "To add a transition:"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:84
msgid "Adjust your clips so that they overlap."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:85
msgid ""
"Hold the mouse wheel over the top clip bottom corner, and click the purple "
"circle appearing (the :ref:`status_bar` will say \"Click to add a composition"
"\"), or alternatively right-click either clip and select :menuselection:"
"`Insert a composition... --> Wipe`."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:87
msgid ""
"The default transition is a dissolve, to change that select (click) the "
"composition bar to show the effect/composition stack window."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:89
msgid ""
"The transition, when added this way, will cover the overlapping area between "
"the clips."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:91
msgid "You can:"
msgstr ""

#: ../../effects_and_compositions/transitions.rst:93
msgid ""
"Select a composition track. The default is \"Automatic\" which is likely the "
"correct choice in this case."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:94
msgid "Select a wipe."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:95
msgid "Enable :guilabel:`Inverse` to inverse the wipe."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:96
msgid ""
"Enable :guilabel:`Revert` to revert the order of the videos in the "
"transition (which you probably do not want)."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:98
msgid ""
"If you move or adjust your clips after adding the transition, you need to "
"refit it manually if desired. You can drag the entire transition with the "
"select tool, and adjust its duration by dragging either end."
msgstr ""

#: ../../effects_and_compositions/transitions.rst:101
msgid "Available Transitions"
msgstr ""
