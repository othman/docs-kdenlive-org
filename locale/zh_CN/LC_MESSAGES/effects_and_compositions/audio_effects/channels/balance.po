msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-15 00:36+0000\n"
"PO-Revision-Date: 2023-12-30 13:42\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_effects_and_compositions___audio_effects___channels___balance."
"pot\n"
"X-Crowdin-File-ID: 48472\n"

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:1
msgid "Kdenlive Audio Effects - Balance"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, audio "
"effects, balance"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:14
msgid "Balance"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:24
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:26
msgid "Yes"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:28
msgid "avfilter"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:30
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:32
#: ../../effects_and_compositions/audio_effects/channels/balance.rst:34
msgid "No"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:40
#: ../../effects_and_compositions/audio_effects/channels/balance.rst:52
msgid "Description"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:41
msgid "Adjusts the left/right balance."
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:44
msgid "Parameters"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:50
msgid "Parameter"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:51
msgid "Value"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:54
msgid "**Balance**"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:55
msgid "Integer"
msgstr ""

#: ../../effects_and_compositions/audio_effects/channels/balance.rst:56
msgid "Adjusts the balance: 0 - left; 500 - center; 1000 - right"
msgstr ""
