msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-12-30 13:42\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/documentation-docs-kdenlive-org/"
"docs_kdenlive_org_importing_and_assets_management___projects_and_files___backup."
"pot\n"
"X-Crowdin-File-ID: 26023\n"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:16
msgid "Backup"
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:24
msgid ""
"The Backup widget, found in :menuselection:`Project --> Open Backup File` "
"allows you to restore a previous version of your project file."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:27
msgid ""
"In case something went wrong (corrupted project file, unwanted change, ...), "
"you can now restore a previous version of the file using this feature. Just "
"select the version you want and click :menuselection:`Open`."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:29
msgid ""
"The backup files are automatically created each time you save your project. "
"This means that if you save your project every hour, the backup widget will "
"show you a list of all the saved files, with a small image of the timeline "
"at the time you saved the project."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:32
msgid ""
"**Kdenlive** keeps up to 20 versions of your project file in the last hour, "
"20 versions from the current day, 20 versions in the last 7 days and 20 "
"older versions, which should be sufficient to recover from any problem."
msgstr ""

#: ../../importing_and_assets_management/projects_and_files/backup.rst:34
msgid ""
"More details how to get backups when the project file version is upgraded "
"see :ref:`project`."
msgstr ""
