# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-08-09 12:39+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:1
msgid "Kdenlive Video Effects - Freeze"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, motion, freeze"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:19
msgid "Freeze"
msgstr "Zmrazení"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:30
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:32
#: ../../effects_and_compositions/video_effects/motion/freeze.rst:40
#: ../../effects_and_compositions/video_effects/motion/freeze.rst:42
msgid "No"
msgstr "Ne"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:34
msgid "MLT"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:36
msgid "freeze"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:38
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:48
#: ../../effects_and_compositions/video_effects/motion/freeze.rst:62
msgid "Description"
msgstr "Popis"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:49
msgid ""
"This effect causes the video to freeze. By default, the clip will be frozen "
"for its entire length when the effect is added to the clip. To change this, "
"check either the :guilabel:`Freeze Before` or :guilabel:`Freeze After` "
"option and move the :guilabel:`Freeze at` slider to the time where you want "
"the freeze to start or end. The audio in the video plays for the entire "
"length, i.e. the **Freeze** effect does *not* affect the audio."
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:53
msgid "Parameters"
msgstr "Parametry"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:60
msgid "Parameter"
msgstr "Parametr"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:61
msgid "Value"
msgstr "Hodnota"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:63
msgid "Freeze at"
msgstr "Zmrazit na"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:65
msgid ""
"Set the position via the slider or the time code (using the format hh:mm:ss:"
"ff)"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:66
msgid "Freeze Before"
msgstr "Zmrazit před"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:68
msgid ""
"If checked, freezes the video from the start of the clip to the set :"
"guilabel:`Freeze at` position. Default is **off**"
msgstr ""

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:69
msgid "Freeze After"
msgstr "Zmrazit po"

#: ../../effects_and_compositions/video_effects/motion/freeze.rst:71
msgid ""
"If checked, freezes the video from set :guilabel:`Freeze at` position to the "
"end of the clip. Default is **off**"
msgstr ""
