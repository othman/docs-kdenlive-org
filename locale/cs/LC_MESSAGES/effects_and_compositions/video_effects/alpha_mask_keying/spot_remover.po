# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Vit Pelcak <vit@pelcak.org>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-06-09 16:05+0200\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.04.1\n"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:1
msgid "Kdenlive Video Effects - Spot Remover"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, alpha, mask, keying, spot, remove"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:22
msgid "Spot Remover"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:33
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:35
msgid "Yes"
msgstr "Ano"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:37
msgid "MLT"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:39
msgid "|spot_remover|"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:41
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:43
#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:45
msgid "No"
msgstr "Ne"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:51
msgid "Description"
msgstr "Popis"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:52
msgid ""
"Replace an area with interpolated pixels. The new pixel values are "
"interpolated from the nearest pixels immediately outside of the specified "
"area. Adjust the size and the position according to your needs using the red "
"rectangle\\ [1]_ in the project monitor."
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:56
msgid "Example"
msgstr "Příklad"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:57
msgid ""
"This example shows how to use the Spot Remover effect: A bird is crossing "
"the viewport and is removed from the final video. For demonstration purposes "
"the original video is on the left, the one with the bird removed is on the "
"right. The bird's position is indicated by the yellow circle."
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:64
msgid "Spot Remover effect panel with example"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:66
msgid ""
"Note the keyframe panel and the keyframe positions in the Project Monitor. "
"Edit Mode must be enabled to see the red edit points and handles."
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/spot_remover.rst:72
msgid "Enable |edit-mode|\\ :guilabel:`Edit Mode` see the red rectangle."
msgstr ""
