msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-04-25 21:50+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Mac Kdenlive\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: ../../user_interface/menu/settings_menu/configure_language.rst:1
msgid "The Kdenlive User Manual"
msgstr "O Manual de Utilizador do Kdenlive"

#: ../../user_interface/menu/settings_menu/configure_language.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, free, "
"help, learn, setting, configure, adjustment, change setting, language"
msgstr ""
"KDE, Kdenlive, documentação, manual do utilizador, editor de vídeo, código "
"aberto, livre, aprender, configurar, configuração, ajuste mudar a "
"configuração, língua"

#: ../../user_interface/menu/settings_menu/configure_language.rst:21
msgid "Configure Language"
msgstr "Configurar a Língua"

#: ../../user_interface/menu/settings_menu/configure_language.rst:27
msgid "Setting Kdenlive to your preferred language."
msgstr "Configura o Kdenlive para a sua língua preferida."

#: ../../user_interface/menu/settings_menu/configure_language.rst:31
msgid "Since 23.04 the language switch works properly on Windows and on Mac."
msgstr ""
"Desde o 23.04, a mudança de língua funciona correctamente no Windows e no "
"Mac."
