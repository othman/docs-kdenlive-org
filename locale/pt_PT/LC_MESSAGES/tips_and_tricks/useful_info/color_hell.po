# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2022-07-06 12:17+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Ffmpeg tablets Android AZ Screen Recording HD MLT ips\n"
"X-POFile-SpellExtra: IPS ffmpeg mp crf gravacao ecra clip ffprobe ITU vf\n"
"X-POFile-SpellExtra: colormatrix bt Grrr Exchange PAL NTSC cromaticidades\n"
"X-POFile-SpellExtra: mediainfo SD grep error showstreams color colorrange\n"
"X-POFile-SpellExtra: tv colorspace bg colortransfer smpte colorprimaries\n"
"X-POFile-SpellExtra: hipster colortrc gamma screen rec Kdenlive\n"

#: ../../tips_and_tricks/useful_info/color_hell.rst:1
#, fuzzy
#| msgid "Color Hell: Ffmpeg Transcoding and Preserving BT.601"
msgid ""
"Kdenlive Tips & Tricks - Color Hell: ffmpeg Transcoding and Preserving BT.601"
msgstr "Inferno de Cores: Conversão do Ffmpeg e Preservação do BT.601"

#: ../../tips_and_tricks/useful_info/color_hell.rst:1
msgid ""
"KDE, Kdenlive, tips, tricks, tips & tricks, useful information, ffmpeg, "
"transcoding, bt.601, documentation, user manual, video editor, open source, "
"free, learn, easy"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:38
msgid "Color Hell: Ffmpeg Transcoding and Preserving BT.601"
msgstr "Inferno de Cores: Conversão do Ffmpeg e Preservação do BT.601"

#: ../../tips_and_tricks/useful_info/color_hell.rst:40
#, fuzzy
#| msgid ""
#| "From time to time, you may get into weird digital video territory quite "
#| "unexpectedly. For instance, you just want to cut some screen records made "
#| "on mobile devices, such as tablets or mobile phones. What could possibly "
#| "go wrong? Colors, for instance…"
msgid ""
"From time to time, you may get into weird digital video territory quite "
"unexpectedly. For instance, you just want to cut some screen records made on "
"mobile devices, such as tablets or mobile phones, only to find out that "
"something is wrong with the colors."
msgstr ""
"De tempos a tempos, poderá entrar em território de vídeo digital estranhos "
"de forma inesperada. Por exemplo, irá querer cortar algumas gravações do "
"ecrã feitas com dispositivos móveis, como 'tablets' ou telemóveis. O que é "
"que poderá correr mal? Cores, por exemplo…"

#: ../../tips_and_tricks/useful_info/color_hell.rst:44
#, fuzzy
#| msgid "“Run-of-the-Mill” Footage"
msgid "Run-of-the-Mill Footage"
msgstr "Filmagem “Aborrecida”"

#: ../../tips_and_tricks/useful_info/color_hell.rst:46
msgid ""
"The drama starts with screen recording footage that seems quite innocent and "
"normal at first sight. It may have been recorded on Android 7 devices using "
"a screen recording app (such as «AZ Screen Recording», but not the “Pro” "
"fake). And this footage has two slightly unusual properties:"
msgstr ""
"O drama começa com as filmagens de gravação do ecrã que aparecem bastante "
"inocentes e normais à primeira vista. Poderá ter sido gravado em "
"dispositivos de Android 7 com uma aplicação de gravação do ecrã (como o «AZ "
"Screen Recording», mas não a versão falsa “Pro”). E esta filmagem tem duas "
"propriedades ligeiramente fora do normal:"

#: ../../tips_and_tricks/useful_info/color_hell.rst:48
msgid "a *highly variable frame rate*,"
msgstr "uma *taxa de imagens altamente variável*,"

#: ../../tips_and_tricks/useful_info/color_hell.rst:49
msgid ""
"it is using :abbr:`BT.601 (A standard from 1982 defining how RGB color "
"primaries get turned into the YCbCr channels used by modern codecs)`\\ "
"[#f1]_, instead of :abbr:`BT.709 (A standard from 1990 which does the same "
"at BT.601 but the transfer coefficients are slightly different)`\\ [#f2]_ "
"like so much HD footage these days."
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:51
msgid "Should cause no problems, right? Well…"
msgstr "Não deveria causar problemas, certo? Bem…"

#: ../../tips_and_tricks/useful_info/color_hell.rst:53
#, fuzzy
#| msgid ""
#| "As it turns out, Kdenlive’s media engine `MLT <https://www.mltframework."
#| "org/>`_ can exhibit some issues with video footage that has a highly "
#| "variable frame rate, such as between 0.001 and 100+ fps. The symptoms are "
#| "subtle, yet endanger production quality: it seems as if MLT may well pick "
#| "a future frame which is way off in regions with a low framerate. While "
#| "this isn’t an issue for a suitably high framerate, this causes odd "
#| "results in other places. For instance, in my productions user touch "
#| "interaction shows up even a few seconds before the interaction will "
#| "appear. This is probably caused by a very low fps during the inactivity "
#| "period just before the user interaction."
msgid ""
"As it turns out, Kdenlive's media engine |mlt| can exhibit some issues with "
"video footage that has a highly variable frame rate, such as between 0.001 "
"and 100+ :abbr:`fps (frames per second)`. The symptoms are subtle, yet "
"endanger production quality: it seems as if MLT may well pick a future frame "
"which is way off in regions with a low framerate. While this is not an issue "
"for a suitably high framerate, this causes odd results in other places. For "
"instance, user touch interaction shows up even a few seconds before the "
"interaction will appear. This is probably caused by a very low fps during "
"the inactivity period just before the user interaction."
msgstr ""
"Como se nota, o motor multimédia do Kdenlive `MLT <https://www.mltframework."
"org/>`_ pode exibir alguns problemas com as filmagens de vídeo que tenham "
"uma faixa de imagens altamente variável, como um intervalo entre 0,001 e "
"100+ ips. Os sintomas são subtis, mas prejudicam em grande medida a "
"qualidade da produção: é como se o MLT conseguisse extrair uma imagem no "
"futuro que está bastante fora das regiões com uma taxa de imagens baixa. "
"Embora isto não seja um problema para uma taxa de imagem adequadamente "
"elevada, isto irá originar resultados com problemas noutros locais. Por "
"exemplo, nas minhas produções, a interacção táctil do utilizador aparece "
"mesmo alguns segundos antes de a interacção aparecer. Isto é provavelmente "
"causado por uma taxa de IPS muito baixa durante o período de inactividade "
"logo antes da interacção com o utilizador."

#: ../../tips_and_tricks/useful_info/color_hell.rst:55
#, fuzzy
#| msgid ""
#| "Alas, transcoding to a fixed frame rate surely is one of `ffmpeg’s "
#| "<https://www.ffmpeg.org/>`_ easy tasks (this example assumes a constant "
#| "project frame rate of 25 fps):"
msgid ""
"Transcoding to a fixed frame rate surely is one of |ffmpeg|'s easy tasks "
"(this example assumes a constant project frame rate of 25 fps):"
msgstr ""
"Bem, a conversão para uma taxa de imagens fixa será certamente uma das "
"tarefas simples do `ffmpeg <https://www.ffmpeg.org/>`_ (este exemplo assume "
"uma taxa de imagens do projecto constante de 25 IPS):"

#: ../../tips_and_tricks/useful_info/color_hell.rst:61
msgid ""
"The constant frame rate cures the issues mentioned above, so the results are "
"as to be expected. Except…"
msgstr ""
"A taxa de imagens constante cura os problemas mencionados acima, pelo que os "
"resultados seriam os esperados. Excepto…"

#: ../../tips_and_tricks/useful_info/color_hell.rst:65
msgid "Easy Transcoding: Color Me Bad"
msgstr "Conversão Simples: Pinta-me Mal"

#: ../../tips_and_tricks/useful_info/color_hell.rst:72
msgid ""
"Unfortunately, the resulting video now shows shifted colors! It might not be "
"too obvious in the first place, but it can be quite prominent when you work "
"more with your footage. And it gets clearly visible to your audience in case "
"you are going to mix this footage side-by-side with further processed "
"versions of it, such as extracted frames for stills."
msgstr ""
"Infelizmente, o vídeo resultante agora mostra as cores deslocadas! Poderá "
"não ser demasiado óbvio à primeira vista, mas poder-se-á tornar demasiado "
"notório à medida que trabalhar mais sobre estas filmagens. Para além disso, "
"fica claramente visível para a sua audiência no caso de fazer alguma mistura "
"destas filmagens lado-a-lado com versões já processadas da mesma, como por "
"exemplo algumas imagens estáticas extraídas."

#: ../../tips_and_tricks/useful_info/color_hell.rst:74
#, fuzzy
#| msgid ""
#| "A more close inspection, either using Kdenlive’s built-in clip properties "
#| "pane or `ffprobe <https://www.ffmpeg.org/ffprobe.html>`_, reveals that "
#| "the *transcoded file* **lacks the BT.601 color profile indication**. Yet, "
#| "ffmpeg did *not transform the colors* at all during transcoding, and "
#| "simply dropped the correct color profile information!"
msgid ""
"A closer inspection either using Kdenlive's built-in clip properties pane or "
"|ffprobe| reveals that the *transcoded file* **lacks the BT.601 color "
"profile indication**. Yet, |ffmpeg| did *not transform the colors* at all "
"during transcoding, and simply dropped the correct color profile information!"
msgstr ""
"Uma inspecção mais detalhada, seja com o painel de propriedades do 'clip' "
"incorporado no Kdenlive ou com o `ffprobe <https://www.ffmpeg.org/ffprobe."
"html>`_, o mesmo revela que o *ficheiro convertido* **não tem a indicação do "
"perfil de cores BT.601**. De qualquer forma, o 'ffmpeg' *não transformou as "
"cores* de todo durante a conversão, descartando simplesmente as informações "
"do perfil de cores correcto!"

#: ../../tips_and_tricks/useful_info/color_hell.rst:79
msgid "Makeshift Measures"
msgstr "Medidas de Salvaguarda"

#: ../../tips_and_tricks/useful_info/color_hell.rst:86
msgid "Clip Properties color space override"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:88
#, fuzzy
#| msgid ""
#| "Of course, there’s always Kdenlive’s ability to overwrite source clip "
#| "properties using the built-in clip properties pane."
msgid ""
"Of course, there is always Kdenlive's ability to overwrite source clip "
"properties using the built-in clip properties :term:`widget`."
msgstr ""
"Obviamente, existe sempre a capacidade de o Kdenlive substituir as "
"propriedades do 'clip' de origem, usando a área de propriedades do 'clip' "
"incorporada."

#: ../../tips_and_tricks/useful_info/color_hell.rst:90
#, fuzzy
#| msgid ""
#| "Simply select the transcoded video clip in the project bin. Then go to "
#| "the clip properties pane and select its “Force Properties” tab which "
#| "shows a *writing pen*. Check “Colorspace” and then select “ITU-R 601”. "
#| "Kdenlive now applies the correct color profile."
msgid ""
"Simply select the transcoded video clip in the project bin. Then go to clip "
"properties and select its “Force Properties” tab |document-edit|. Check :"
"guilabel:`Colorspace` and then select **ITU-R 601**. Kdenlive now applies "
"the correct color profile."
msgstr ""
"Basta seleccionar o 'clip' de vídeo convertido no grupo do projecto. Depois "
"vá à área de propriedades do 'clip' e seleccione a página “Forçar as "
"Propriedades”, que mostra uma *caneta*. Assinale o “Espaço de cores” e "
"seleccione depois o “ITU-R 601”. O Kdenlive agora aplica o perfil de cores "
"correcto."

#: ../../tips_and_tricks/useful_info/color_hell.rst:94
#, fuzzy
#| msgid ""
#| "While very easy, this method has its limitations; it’s fine while you "
#| "keep working *solely inside the Kdenlive editor and its MLT renderer*. "
#| "But as soon as you need to pull in external video tools, such as *ffmpeg* "
#| "for image extraction…, you will loose: these tools don’t know about "
#| "Kdenlive’s source clip property overrides. We thus need to get the "
#| "correct color profile information right into the transcoded video files "
#| "themselves."
msgid ""
"While very easy, this method has its limitations: It is fine while you keep "
"working solely inside the Kdenlive editor and its MLT renderer. But as soon "
"as you need to pull in external video tools, such as |ffmpeg| for image "
"extraction you will loose because these tools do not know about Kdenlive's "
"source clip property overrides. We thus need to get the correct color "
"profile information right into the transcoded video files themselves."
msgstr ""
"Embora seja bastante simples, este método tem as suas limitações;  é óptimo "
"enquanto continuar a trabalhar *exclusivamente dentro do editor Kdenlive e "
"do seu sistema de desenho MLT*. Mas, assim que precisar de usar ferramentas "
"de vídeo externas, como o *ffmpeg* para a extracção de imagens…, ficará a "
"perder: estas ferramentas não irão conhecer as substituições dos valores do "
"'clip' de origem. Para isso, é necessário obter a informação do perfil de "
"cores correcto directamente os ficheiros de vídeo convertidos."

#: ../../tips_and_tricks/useful_info/color_hell.rst:97
msgid "Preserving BT.601 in Transcoding"
msgstr "Preservar o BT.601 na Conversão"

#: ../../tips_and_tricks/useful_info/color_hell.rst:99
msgid ""
"To make this matter worse, the seemingly obvious color profile transformation"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:105
#, fuzzy
#| msgid ""
#| "To make this matter worse, the seemingly obvious color profile "
#| "transformation ``-vf colormatrix=bt601:bt601`` simply doesn’t work: "
#| "ffmpeg complains about not being to transform between the same input and "
#| "output color profile. *Grrr*."
msgid ""
"simply does not work: |ffmpeg| complains about not being able to transform "
"between the same input and output color profile."
msgstr ""
"Para piorar esta situação, a transformação do perfil de cores aparentemente "
"óbvia ``-vf colormatrix=bt601:bt601`` simplesmente não funciona: o 'ffmpeg' "
"queixa-se de não poder converter entre o mesmo perfil de cores de entrada e "
"de saída. *Grrr*."

#: ../../tips_and_tricks/useful_info/color_hell.rst:107
msgid ""
"The missing puzzle piece can be found on Stack Exchange's Video Production "
"Q&A site in a post from 2015 asking \"|explicitly_tag|\"."
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:109
#, fuzzy
#| msgid ""
#| "There’s a catch to watch out for: BT.601 comes in PAL and NTSC flavors "
#| "which feature slightly different primary chromaticities, transfer curves, "
#| "and colorspaces. So check your raw footage first using ``ffprobe`` (or "
#| "``mediainfo``) which one has been used during recording in your case. "
#| "Please note that it doesn’t matter that your screen recording hasn’t "
#| "standard definition (SD) resolution at all, but it does matter when it "
#| "comes to encoding color."
msgid ""
"There is a catch to watch out for: BT.601 comes in :abbr:`PAL (Phase "
"Alternating Line - a colour encoding system for analogue television)` and :"
"abbr:`NTSC (National Television Standard Committee - defined the TV system "
"used in the United States, Japan and many other countries)` flavors which "
"feature slightly different primary chromaticities, transfer curves, and "
"colorspaces. So check your raw footage first using |ffprobe| (or |"
"mediainfo|) which one has been used during recording in your case. Please "
"note that it does not matter that your screen recording has not standard "
"definition (SD) resolution at all, but it does matter when it comes to "
"encoding color."
msgstr ""
"Existe um detalhe a ter em conta: o BT.601 vem com as variantes para PAL e "
"NTSC que usam cromaticidades primárias, curvas de transferência e espaços de "
"cores ligeiramente diferentes. Como tal, veja a sua filmagem em bruto "
"primeiro com  o ``ffprobe`` (ou o ``mediainfo``),  dependendo de qual foi "
"usado durante a gravação no seu caso. Lembre-se que não interessa se a sua "
"gravação do ecrã tem uma definição-padrão (SD) de todo, mas isso interessa "
"no que respeita à cor da codificação."

#: ../../tips_and_tricks/useful_info/color_hell.rst:112
msgid "PAL and NTSC DNA"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:114
#, fuzzy
#| msgid ""
#| "So how do we find out if a given video recording file, say ``raw.mp4``, "
#| "uses the PAL or NTSC color space? Of course, ``ffprobe`` comes to our "
#| "rescue. But in order to not get lost in all the nitty-gritty details "
#| "``ffprobe`` will throw at you, we need to tame it using a few options and "
#| "``grep``:"
msgid ""
"So how do we find out if a given video recording file, say ``raw.mp4``, uses "
"the PAL or NTSC color space? Of course, |ffprobe| comes to our rescue. But "
"in order to not get lost in all the nitty-gritty details |ffprobe| will "
"throw at you, we need to tame it using a few options and :abbr:`grep (A Unix "
"command-line utility for searching plain-text data sets for lines that match "
"a regular expression)`:"
msgstr ""
"Então, como é que descobrimos se um dado ficheiro de vídeo de filmagem, como "
"por exemplo o ``bruto.mp4``, usa o espaço de cores PAL ou NTSC? Obviamente, "
"o ``ffprobe`` vem para nos salvar. Contudo, para não se perder em todos os "
"detalhes estranhos que o ``ffprobe`` lhe irá apresentar, é preciso controlá-"
"lo um pouco, usando apenas algumas opções e o comando ``grep``:"

#: ../../tips_and_tricks/useful_info/color_hell.rst:120
msgid "This should give you something along these lines:"
msgstr "Isto deve-lhe indicar algo do género destas linhas:"

#: ../../tips_and_tricks/useful_info/color_hell.rst:129
msgid ""
"The line ``color_space=...`` tells us whether we are dealing with PAL "
"(bt470bg) or NTSC (smpte170m)."
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:132
msgid "PAL"
msgstr "PAL"

#: ../../tips_and_tricks/useful_info/color_hell.rst:134
#, fuzzy
#| msgid ""
#| "If it’s **PAL chromaticities** (``=bt470bg``), we then need to transcode "
#| "as follows:"
msgid ""
"If it is **PAL chromaticities** (``color_space=bt470bg``), we then need to "
"transcode as follows:"
msgstr ""
"Se for as **cromaticidades do PAL** (``=bt470bg``), então teremos de "
"converter da seguinte forma:"

#: ../../tips_and_tricks/useful_info/color_hell.rst:143
msgid "NTSC"
msgstr "NTSC"

#: ../../tips_and_tricks/useful_info/color_hell.rst:145
#, fuzzy
#| msgid ""
#| "For **NTSC chromaticities** (``=smpte170m``), we’ll need a different set "
#| "of primaries, transfer curve, and colorspace:"
msgid ""
"For **NTSC chromaticities** (``color_space=smpte170m``), we will need a "
"different set of primaries, transfer curve, and colorspace:"
msgstr ""
"Para as **cromaticidades do NTSC** (``=smpte170m``), será necessário um "
"conjunto diferente de componentes primárias, curva de transferência e espaço "
"de cores:"

#: ../../tips_and_tricks/useful_info/color_hell.rst:158
#, fuzzy
#| msgid ""
#| "In any case, Kdenlive/MLT now correctly see the transcoded video using "
#| "the BT.601 color profile. In addition, other media tools correctly detect "
#| "the color profile too – unless they are broken in that they don’t "
#| "understand BT.601 at all."
msgid ""
"In any case, Kdenlive/MLT now correctly see the transcoded video using the "
"BT.601 color profile. In addition, other media tools correctly detect the "
"color profile too - unless they are broken in that they do not understand "
"BT.601 at all."
msgstr ""
"Em qualquer dos casos, o Kdenlive/MLT agora irá ver correctamente o vídeo "
"convertido com o perfil de cores BT.601. Para além disso, as outras "
"ferramentas multimédia passam a detectar correctamente o perfil de cores "
"também - a menos que sejam limitados na medida em que não compreendem o "
"BT.601 de todo."

#: ../../tips_and_tricks/useful_info/color_hell.rst:163
msgid "Notes"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:177
msgid "**Sources**"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:177
msgid ""
"The original text was submitted by user *TheDiveO* to the now defunct "
"kdenlive.org blog. For this documentation it has been lifted from |"
"kdenlive_org| and adapted to match the overall style."
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:181
msgid "For more details see the Wikipedia article about |bt601|"
msgstr ""

#: ../../tips_and_tricks/useful_info/color_hell.rst:182
msgid "For more details see the Wikipedia article about |bt709|"
msgstr ""

#~ msgid ""
#~ "it’s using `BT.601 <https://en.wikipedia.org/wiki/Rec._601>`_ , instead "
#~ "of BT.709 like so much HD footage these days."
#~ msgstr ""
#~ "está a usar o `BT.601 <https://en.wikipedia.org/wiki/Rec._601>`_ , em vez "
#~ "do  BT.709 como acontece nas filmagens em HD nestes dias."

#~ msgid "$ ``ffmpeg -i raw.mp4 -r 25 -crf 18 screen-rec.mp4``"
#~ msgstr "$ ``ffmpeg -i bruto.mp4 -r 25 -crf 18 gravacao-ecra.mp4``"

#~ msgid ""
#~ "It took quite some extensive searching until I found the missing puzzle "
#~ "piece on Stack Exchange’s Video Production Q&A site: `ffmpeg: explicitly "
#~ "tag h.264 as bt.601, rather than leaving unspecified? <https://video."
#~ "stackexchange.com/questions/16840/ffmpeg-explicitly-tag-h-264-as-bt-601-"
#~ "rather-than-leaving-unspecified>`_"
#~ msgstr ""
#~ "Depois de algumas pesquisas abrangentes, descobriu-se a peça do puzzle em "
#~ "falta na página de Perguntas & Respostas de Produção de Vídeo do Stack "
#~ "Exchange: o `ffmpeg: marca explicitamente o 'h.264' como 'bt.601', em vez "
#~ "de deixar por preencher? <https://video.stackexchange.com/questions/16840/"
#~ "ffmpeg-explicitly-tag-h-264-as-bt-601-rather-than-leaving-unspecified>`_"

#~ msgid "I’m Not Quite Dead Yet: PAL and NTSC DNA"
#~ msgstr "Ainda não Estou Morto: o ADN do PAL e NTSC"

#~ msgid "$ ``ffprobe -v error -show_streams raw.mp4 | grep color_``"
#~ msgstr "$ ``ffprobe -v error -show_streams bruto.mp4 | grep color_``"

#~ msgid ""
#~ "``color_range=tv`` ``color_space=bt470bg`` ``color_transfer=smpte170m`` "
#~ "``color_primaries=bt470bg``"
#~ msgstr ""
#~ "``color_range=tv`` ``color_space=bt470bg`` ``color_transfer=smpte170m`` "
#~ "``color_primaries=bt470bg``"

#~ msgid ""
#~ "Someone surely thought that using a TV standard definition-related BT.601 "
#~ "is a clever idea to record mobile device screens. Must have been a "
#~ "hipster with a old-school tube TV sitting on his desk. Alas, the line "
#~ "``color_space=...`` will tell us whether we’re dealing with PAL "
#~ "(``=bt470bg``) or NTSC (``=smpte170m``)."
#~ msgstr ""
#~ "Alguém deve ter pensado que usar o BT.601, relacionado com as resoluções-"
#~ "padrão de TV são uma boa ideia para gravar ecrãs de dispositivos móveis. "
#~ "Deveria ser um 'hipster' com uma TV antiga de raios catódicos no seu "
#~ "móvel de sala. Aí, a linha ``color_space=...`` indicar-nos-á se estamos a "
#~ "lidar com o PAL (``=bt470bg``) ou NTSC (``=smpte170m``)."

#~ msgid "$ ``ffmpeg -i raw.mp4``"
#~ msgstr "$ ``ffmpeg -i bruto.mp4``"

#~ msgid "``-color_primaries bt470bg -color_trc gamma28 -colorspace bt470bg``"
#~ msgstr "``-color_primaries bt470bg -color_trc gamma28 -colorspace bt470bg``"

#~ msgid "``-r 25 -crf 18 screen-rec.mp4``"
#~ msgstr "``-r 25 -crf 18 screen-rec.mp4``"

#~ msgid ""
#~ "``-color_primaries smpte170m -color_trc smpte170m -colorspace smpte170m``"
#~ msgstr ""
#~ "``-color_primaries smpte170m -color_trc smpte170m -colorspace smpte170m``"
