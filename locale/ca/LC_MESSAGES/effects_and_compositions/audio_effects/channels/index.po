# Translation of docs_kdenlive_org_effects_and_compositions___audio_effects___channels___index.po to Catalan
# Copyright (C) 2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-15 00:36+0000\n"
"PO-Revision-Date: 2023-11-16 15:00+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../effects_and_compositions/audio_effects/channels/index.rst:1
msgid "Kdenlive Audio Effects - Channels"
msgstr "Efectes d'àudio del Kdenlive - Canals"

#: ../../effects_and_compositions/audio_effects/channels/index.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, audio "
"effects, channels"
msgstr ""
"KDE, Kdenlive, documentació, manual d'usuari, editor de vídeo, codi lliure, "
"efectes d'àudio, canals"

#: ../../effects_and_compositions/audio_effects/channels/index.rst:19
msgid "Channels"
msgstr "Canals"

#: ../../effects_and_compositions/audio_effects/channels/index.rst:21
msgid ""
"This category consists of effects and filters for things like balance, "
"panning, and mixdowns."
msgstr ""
"Aquesta categoria consisteix en efectes i filtres per a coses com el balanç, "
"les panoràmiques i les concentracions."

#: ../../effects_and_compositions/audio_effects/channels/index.rst:23
msgid "The following filters and effects are available:"
msgstr "Hi ha disponibles els efectes i filtres següents:"
