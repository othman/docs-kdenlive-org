# Translation of docs_kdenlive_org_effects_and_compositions___video_effects___transform_distort_perspective___rotate_and_shear.po to Catalan
# Copyright (C) 2021-2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-11-03 16:10+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:1
msgid "Kdenlive Video Effects - Rotate and Shear"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, transform, distort, perspective, rotate and shear"
msgstr ""
"KDE, Kdenlive, editor de vídeo, ajuda, aprendre, fàcil, efectes, filtre, "
"efectes de vídeo, transforma, distorsió, perspectiva, gira i inclina"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:18
msgid "Rotate and Shear"
msgstr "Gira i inclina"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:29
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:31
msgid "Yes"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:33
msgid "MLT"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:35
msgid "affine"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:37
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:39
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:41
msgid "No"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:47
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:63
msgid "Description"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:48
msgid "This effect/filter allows to rotate the clip in any three directions."
msgstr ""
"Aquest efecte/filtre permet girar el clip en qualsevol de les tres "
"direccions."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:50
msgid ""
"The effect has keyframes but not for the rotation and the shear degrees. "
"This is controlled by the :guilabel:`Animate` parameters."
msgstr ""
"L'efecte té fotogrames clau, però no per als graus de gir i inclinació. Això "
"està controlat pels paràmetres d':guilabel:`Anima`."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:54
msgid "Parameters"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:61
msgid "Parameter"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:62
msgid "Value"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:64
msgid "Rotate X / Y / Z"
msgstr ""

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:66
#, fuzzy
#| msgid ""
#| "**Rotate X / Y / Z** - Set the amount of rotation in ``degrees * 10``. "
#| "For example: 900 = 90 degrees rotation."
msgid ""
"Set the amount of rotation in ``degrees * 10``. For example: 900 = 90 "
"degrees rotation."
msgstr ""
"**Gir X / Y / Z**: Estableix la quantitat de gir en ``graus * 10``. Per "
"exemple: 900 = 90 graus de gir."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:67
msgid "Animate Rotate X / Y / Z"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:69
#, fuzzy
#| msgid ""
#| "**Animate Rotate X / Y / Z** - Same as :guilabel:`Rotate`. The clip is "
#| "rotated by that amount every frame."
msgid ""
"Same as :guilabel:`Rotate`. The clip is rotated by that amount every frame."
msgstr ""
"**Gir animat X / Y / Z**: Igual que :guilabel:`Gir`. El clip es gira en "
"aquesta quantitat cada fotograma."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:70
msgid "Shear X / Y"
msgstr ""

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:72
#, fuzzy
#| msgid ""
#| "**Shear X / Y** - Set the amount of shear in ``degrees * 10``. For "
#| "example: 450 = 45 degrees shear (diagonal)."
msgid ""
"Set the amount of shear in ``degrees * 10``. For example: 450 = 45 degrees "
"shear (diagonal)."
msgstr ""
"**Inclinació X / Y**: Estableix la quantitat d'inclinació en ``graus * 10``. "
"Per exemple: 450 = 45 graus d'inclinació (diagonal)."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:73
msgid "Animate Shear X / Y"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:75
#, fuzzy
#| msgid ""
#| "**Animate Shear X / Y** - Same as :guilabel:`Shear`. The clip is sheared "
#| "by that amount every frame."
msgid ""
"Same as :guilabel:`Shear`. The clip is sheared by that amount every frame."
msgstr ""
"**Inclinació animada X / Y**: Igual que :guilabel:`Inclinació`. El clip "
"s'inclina aquesta quantitat cada fotograma."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:76
msgid "X / Y / W / H / Size"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:78
#, fuzzy
#| msgid ""
#| "**X / Y / W / H / Size** - Allows to move (X, Y) and zoom (Size) the clip "
#| "at the same time"
msgid "Allows to move (X, Y) and zoom (Size) the clip at the same time"
msgstr ""
"**X / Y / W / H / Mida**: Permet moure (X, Y) i ampliar (Mida) el clip al "
"mateix temps"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:79
msgid "Background Color"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:81
#, fuzzy
#| msgid ""
#| "**Background Color** - Define the background color to be used when "
#| "rotating or shearing reveals the background. Default is Alpha."
msgid ""
"Define the background color to be used when rotating or shearing reveals the "
"background. Default is Alpha."
msgstr ""
"**Color de fons**: Defineix el color de fons que s'utilitzarà quan en giri o "
"inclinar es mostri el fons. De manera predeterminada és Alfa."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:85
msgid ""
"The axes for rotation and shear are different than for moving a clip. The "
"latter uses the monitor plane: X = width or horizontal axis, Y = height or "
"vertical axis. For rotation and shear the plane is tilted by 90 degrees "
"\"into the monitor\" and rotated 90 degrees clockwise around the vertical "
"axis. This results in the X axis coming out of/going into the monitor, the Y "
"axis being along the width of the monitor and the Z axis being along the "
"height of the monitor. See image below."
msgstr ""
"Els eixos de gir i inclinació són diferents dels de moure un clip. Aquest "
"últim utilitza el pla del monitor: X = amplada o eix horitzontal, Y = alçada "
"o eix vertical. Per al gir i la inclinació el pla s'inclina 90 graus «en el "
"monitor» i gira 90 graus en sentit horari al voltant de l'eix vertical. Això "
"provoca que l'eix X surti o entri al monitor, l'eix Y està al llarg de "
"l'amplada del monitor i l'eix Z al llarg de l'alçada del monitor. Vegeu la "
"imatge de sota."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_and_shear.rst:93
msgid "Rotate and Shear effect axis visualization"
msgstr "Visualització de l'eix de l'efecte de gir i inclinació"

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using rotate and shear "
#~ "effect"
#~ msgstr ""
#~ "Feu les primeres passes amb l'editor de vídeo Kdenlive, fent servir "
#~ "l'efecte gira i inclina"

#~ msgid "Rotate and Shear effect"
#~ msgstr "Efecte gira i inclina"
