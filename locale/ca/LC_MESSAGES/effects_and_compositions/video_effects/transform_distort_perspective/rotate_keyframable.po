# Translation of docs_kdenlive_org_effects_and_compositions___video_effects___transform_distort_perspective___rotate_keyframable.po to Catalan
# Copyright (C) 2021-2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-11-03 16:10+0100\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:1
#, fuzzy
#| msgid "Rotate (keyframable)"
msgid "Kdenlive Video Effects - Rotate (keyframable)"
msgstr "Gir (amb fotogrames clau)"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, transform, distort, perspective, rotate keyframable"
msgstr ""
"KDE, Kdenlive, editor de vídeo, ajuda, aprendre, fàcil, efectes, filtre, "
"efectes de vídeo, transforma, distorsió, perspectiva, gir amb fotogrames clau"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:17
msgid "Rotate (keyframable)"
msgstr "Gir (amb fotogrames clau)"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:28
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:30
msgid "Yes"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:32
msgid "MLT"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:34
msgid "affine"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:36
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:38
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:40
msgid "No"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:46
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:60
msgid "Description"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:47
msgid ""
"This effect/filter rotates the clip in any of the three directions: X, Y, "
"and Z."
msgstr ""
"Aquest efecte/filtre gira el clip en qualsevol de les tres direccions: X, Y "
"i Z."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:51
msgid "Parameters"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:58
msgid "Parameter"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:59
msgid "Value"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:61
msgid "Disable repeat"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:62
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:65
msgid "Switch"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:63
#, fuzzy
#| msgid ""
#| "**Disable repeat** - When animating properties with keyframes, whether to "
#| "repeat the animation after it reaches the last key frame. Default is "
#| "**on**."
msgid ""
"When animating properties with keyframes, whether to repeat the animation "
"after it reaches the last key frame. Default is **on**."
msgstr ""
"**Desactiva la repetició**: Quan s'animen les propietats amb fotogrames "
"clau, si s'ha de repetir l'animació després d'arribar a l'últim fotograma "
"clau. El valor predeterminat és **activat**."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:64
msgid "Disable mirror"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:66
#, fuzzy
#| msgid ""
#| "**Disable mirror** - When animating properties with keyframes and :"
#| "guilabel:`Disable repeat` is **off**, whether the animation alternates "
#| "between reverses and forwards for each repetition. Default is **on**."
msgid ""
"When animating properties with keyframes and :guilabel:`Disable repeat` is "
"**off**, whether the animation alternates between reverses and forwards for "
"each repetition. Default is **on**."
msgstr ""
"**Desactiva el mirall**: En animar les propietats amb fotogrames clau i :"
"guilabel:`Desactiva la repetició` és **desactivat**, si l'animació "
"s'alternarà entre invertit i directe per a cada repetició. El valor "
"predeterminat és **activat**."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:67
msgid "Rotate X / Y / Z"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:68
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:71
msgid "Integer"
msgstr ""

# skip-rule: t-acc_obe
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:69
#, fuzzy
#| msgid ""
#| "**Rotate X / Y / Z** - Set the amount of rotation in ``degrees * 10``. "
#| "For example: 900 = 90 degrees rotation."
msgid ""
"Set the amount of rotation in ``degrees * 10``. For example: 900 = 90 "
"degrees rotation."
msgstr ""
"**Gir X / Y / Z**: Estableix la quantitat de gir en ``graus * 10``. Per "
"exemple: 900 = 90 graus de gir."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:70
msgid "Offset X / Y"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:72
#, fuzzy
#| msgid "**Offset X / Y** - Horizontal (X) and vertical (Y) offset"
msgid "Horizontal (X) and vertical (Y) offset"
msgstr "**Desplaçament X / Y**: Desplaçament horitzontal (X) i vertical (Y)"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:73
msgid "Background Color"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:74
msgid "Picker"
msgstr ""

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:75
#, fuzzy
#| msgid ""
#| "**Background Color** - Define the background color to be used when "
#| "rotating reveals the background. Default is Alpha."
msgid ""
"Define the background color to be used when rotating reveals the background. "
"Default is Alpha."
msgstr ""
"**Color de fons**: Defineix el color de fons que s'utilitzarà quan en girar "
"es mostri el fons. De manera predeterminada és Alfa."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:79
msgid ""
"The axes for rotation are different than for moving a clip. The latter uses "
"the monitor plane: X = width or horizontal axis, Y = height or vertical "
"axis. For rotation the plane is tilted by 90 degrees \"into the monitor\" "
"and rotated 90 degrees clockwise around the vertical axis. This results in "
"the X axis coming out of/going into the monitor, the Y axis being along the "
"width of the monitor and the Z axis being along the height of the monitor. "
"See image below."
msgstr ""
"Els eixos de gir són diferents dels de moure un clip. Aquest últim utilitza "
"el pla del monitor: X = amplada o eix horitzontal, Y = alçada o eix "
"vertical. Per al gir el pla s'inclina 90 graus «en el monitor» i gira 90 "
"graus en sentit horari al voltant de l'eix vertical. Això provoca que l'eix "
"X surti o entri al monitor, l'eix Y està al llarg de l'amplada del monitor i "
"l'eix Z al llarg de l'alçada del monitor. Vegeu la imatge de sota."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/rotate_keyframable.rst:87
msgid "Rotation axes visualization"
msgstr "Visualització dels eixos de gir"

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using rotate "
#~ "(keyframable) effect"
#~ msgstr ""
#~ "Feu les primeres passes amb l'editor de vídeo Kdenlive, fent servir "
#~ "l'efecte gir (amb fotogrames clau)"

#~ msgid "The effect has keyframes for the rotation and offset."
#~ msgstr "L'efecte té fotogrames clau per al gir i el desplaçament."

#~ msgid "Rotate (keyframable) effect"
#~ msgstr "Efecte gir (amb fotogrames clau)"
