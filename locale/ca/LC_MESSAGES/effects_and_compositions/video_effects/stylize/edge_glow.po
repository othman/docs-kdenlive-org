# Translation of docs_kdenlive_org_effects_and_compositions___video_effects___stylize___edge_glow.po to Catalan
# Copyright (C) 2021-2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2021.
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-11 00:36+0000\n"
"PO-Revision-Date: 2023-08-12 17:17+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**Status**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**Source library**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**Source filter**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**On Master only**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:0
msgid "**Known bugs**"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:1
msgid "Kdenlive Video Effects - Edge Glow"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, stylize, edge glow"
msgstr ""
"KDE, Kdenlive, editor de vídeo, ajuda, aprendre, fàcil, efectes, filtre, "
"efectes de vídeo, estilitza, vora lluent"

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:14
msgid "Edge Glow"
msgstr "Vora lluent"

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:25
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:27
msgid "Yes"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:29
msgid "frei0r"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:31
msgid "edgeglow"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:33
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:35
#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:37
msgid "No"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:43
#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:57
msgid "Description"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:44
msgid "This effect/filter creates an edge glow."
msgstr "Aquest efecte/filtre crea una vora lluent."

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:48
msgid "Parameters"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:55
msgid "Parameter"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:56
msgid "Value"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:58
msgid "Edge lightening threshold"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:59
#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:62
#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:65
msgid "Integer"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:60
#, fuzzy
#| msgid ""
#| "**Edge lightening threshold** - Defines the threshold for edge detection. "
#| "If set to 0 (default), all edges will be detected."
msgid ""
"Defines the threshold for edge detection. If set to 0 (default), all edges "
"will be detected."
msgstr ""
"**Llindar d'aclariment de vores**: Defineix el llindar per a la detecció de "
"les vores. Si s'estableix a 0 (predeterminat), es detectaran totes les vores."

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:61
msgid "Edge brightness upscaling multiplier"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:63
#, fuzzy
#| msgid ""
#| "**Edge brightness upscaling multiplier** - Defines the level of "
#| "brightness in which edges are drawn"
msgid "Defines the level of brightness in which edges are drawn"
msgstr ""
"**Multiplicador d'augment d'escalat de la lluminositat de les vores**: "
"Defineix el nivell de lluminositat en el qual es dibuixen les vores"

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:64
msgid "Non-edge downscaling multiplier"
msgstr ""

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:66
#, fuzzy
#| msgid ""
#| "**Non-edge downscaling multiplier** - Defines the darkening of areas "
#| "between edges"
msgid "Defines the darkening of areas between edges"
msgstr ""
"**Multiplicador de reducció d'escalat de les no vores**: Defineix "
"l'enfosquiment de les àrees entre les vores"

#: ../../effects_and_compositions/video_effects/stylize/edge_glow.rst:69
msgid ""
"For sources with large contiguous areas like a sky or a wall the :guilabel:"
"`Edge lightening threshold` can be used to ignore the small and subtle "
"changes in color or luminance that would otherwise be detected as edges."
msgstr ""
"Per a fonts amb grans àrees contigües com un cel o un mur, es pot utilitzar "
"el :guilabel:`Llindar d'aclariment de vores` per a ignorar els canvis petits "
"i subtils en el color o la luminància que d'una altra manera es detectarien "
"com a vores."

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using edge glow effect"
#~ msgstr ""
#~ "Feu les primeres passes amb l'editor de vídeo Kdenlive, fent servir "
#~ "l'efecte de vora lluent"

#~ msgid "Edge Glow effect"
#~ msgstr "Efecte de vora lluent"
