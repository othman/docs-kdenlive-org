# Translation of docs_kdenlive_org_importing_and_assets_management___capturing.po to Catalan
# Copyright (C) 2021-2023 This_file_is_part_of_KDE
# Licensed under the <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Josep M. Ferrer <txemaq@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: documentation-docs-kdenlive-org\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-10-03 19:19+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../../importing_and_assets_management/capturing.rst:22
msgid "Capturing Video"
msgstr "Captura de vídeo"

#: ../../importing_and_assets_management/capturing.rst:27
msgid ""
"At least Firewire and webcam capture were removed in porting to KDE 5 due to "
"lack of manpower."
msgstr ""
"Com a mínim la captura de Firewire i de càmera web s'han eliminat en "
"l'adaptació al KDE 5 per una manca de mà d'obra."

#: ../../importing_and_assets_management/capturing.rst:30
msgid ""
"**Kdenlive** provides functionality for capturing video from external "
"devices; e.g., Firewire, FFmpeg, Screen Grab and Blackmagic."
msgstr ""
"El **Kdenlive** proporciona característiques per a capturar vídeo des de "
"dispositius externs; p. ex., Firewire, FFmpeg, Captura de pantalla i "
"Blackmagic."

#: ../../importing_and_assets_management/capturing.rst:33
msgid ""
"You configure video capturing from :menuselection:`Settings --> Configure "
"Kdenlive --> Capture` (more on this :ref:`configure_kdenlive`)."
msgstr ""
"Configureu la captura de vídeo a :menuselection:`Arranjament --> Configura "
"el Kdenlive --> Captura` (més en aquesta :ref:`configure_kdenlive`)."

#: ../../importing_and_assets_management/capturing.rst:36
msgid ""
"You define the destination location for your captures by using :"
"menuselection:`Settings --> Configure Kdenlive --> Environment --> Default "
"Folders` (more on this :ref:`configure_kdenlive`)."
msgstr ""
"Definiu la ubicació de destinació per a les vostres captures utilitzant :"
"menuselection:`Arranjament --> Configura el Kdenlive --> Entorn --> Carpetes "
"predeterminades` (més en aquesta :ref:`configure_kdenlive`)."

#: ../../importing_and_assets_management/capturing.rst:39
msgid ""
"To execute a video capture, select the :ref:`ui-monitors` and choose the "
"capture device from the dropdown in the bottom right."
msgstr ""
"Per a executar una captura de vídeo, seleccioneu els :ref:`ui-monitors` i "
"trieu el dispositiu de captura des de la llista desplegable de la part "
"inferior dreta."

#: ../../importing_and_assets_management/capturing.rst:48
msgid "Firewire"
msgstr "Firewire"

#: ../../importing_and_assets_management/capturing.rst:52
msgid ""
"This option is not available in recent versions of Kdenlive. Use dvgrab "
"directly in a terminal to capture video from firewire."
msgstr ""
"Aquesta opció no està disponible en versions recents del Kdenlive. Useu "
"«dvgrab» directament en un terminal per a capturar vídeo des del Firewire."

#: ../../importing_and_assets_management/capturing.rst:55
msgid ""
"This captures video from sources connected via a firewire (also known as -  "
"IEEE 1394 High Speed Serial Bus) card and cable. This functionality uses the "
"`dvgrab <http://linux.die.net/man/1/dvgrab>`_ program and the settings for "
"this can be customized by clicking the spanner icon or choosing  :"
"menuselection:`Settings>Configure Kdenlive`.  See :ref:`configure_kdenlive`."
msgstr ""
"Això captura vídeo de fonts connectades a través d'una targeta i un cable "
"Firewire (també conegut com a IEEE 1394 High Speed Serial Bus). Aquesta "
"funcionalitat utilitza el programa `dvgrab <http://linux.die.net/man/1/"
"dvgrab>`_ i els paràmetres per a això es poden personalitzar fent clic a la "
"icona de l'expansor o escollint :menuselection:`Arranjament>Configura el "
"Kdenlive`. Vegeu :ref:`configure_kdenlive`."

#: ../../importing_and_assets_management/capturing.rst:58
msgid "To perform a capture:"
msgstr "Per a realitzar una captura:"

#: ../../importing_and_assets_management/capturing.rst:61
msgid "Plug in your device to the firewire card and turn it on to play mode"
msgstr ""
"Connecteu el dispositiu a la targeta Firewire i activeu-lo en el mode de "
"reproducció"

#: ../../importing_and_assets_management/capturing.rst:64
msgid "Click the *Connect Button*"
msgstr "Feu clic al *Botó de connexió*"

#: ../../importing_and_assets_management/capturing.rst:70
msgid ""
"Click the Record Button – note it toggles to grey while you are recording"
msgstr ""
"Feu clic al botó d'enregistrament - tingueu en compte que commuta a gris "
"mentre esteu enregistrant"

#: ../../importing_and_assets_management/capturing.rst:73
msgid ""
"Click the Record button again to stop capture. Or click the stop button."
msgstr ""
"Feu clic de nou al botó Enregistra per a aturar la captura. O feu clic al "
"botó d'aturar."

#: ../../importing_and_assets_management/capturing.rst:76
msgid "Once capturing is finished, click the disconnect button"
msgstr "Un cop finalitzada la captura, feu clic al botó de desconnexió"

#: ../../importing_and_assets_management/capturing.rst:83
msgid ""
"In the *Captured Files* dialog, click the import button to have the captured "
"files automatically imported into the project bin."
msgstr ""
"En el diàleg *Fitxers capturats*, feu clic al botó d'importació perquè els "
"fitxers capturats s'importin automàticament a la safata del projecte."

#: ../../importing_and_assets_management/capturing.rst:93
msgid ""
"If your device does not start playing the source device when you click the "
"record button, you may have to start playback on your device manually and "
"then click record."
msgstr ""
"Si el dispositiu no comença a reproduir el dispositiu d'origen quan feu clic "
"al botó d'enregistrament, és possible que hàgiu d'iniciar la reproducció "
"manualment i després fer clic a enregistrar."

#: ../../importing_and_assets_management/capturing.rst:97
msgid "FFmpeg"
msgstr "FFmpeg"

#: ../../importing_and_assets_management/capturing.rst:99
msgid ""
"I believe this captures video from an installed Web Cam using *Video4Linux2*."
msgstr ""
"Crec que això captura vídeo d'una càmera web instal·lada utilitzant "
"*Video4Linux2*."

#: ../../importing_and_assets_management/capturing.rst:104
msgid "Screen Grab"
msgstr "Captura la pantalla"

#: ../../importing_and_assets_management/capturing.rst:106
msgid "This captures video of the PC screen."
msgstr "Això captura vídeo de la pantalla del PC."

#: ../../importing_and_assets_management/capturing.rst:108
msgid "Open screen grab: :menuselection:`View --> Screen Grab`."
msgstr ""
"Obrir la captura de pantalla: :menuselection:`Vista --> Captura de pantalla`."

#: ../../importing_and_assets_management/capturing.rst:110
msgid "Start recording: click the “record” button."
msgstr "Iniciar l'enregistrament: feu clic en el botó «enregistra»."

#: ../../importing_and_assets_management/capturing.rst:113
msgid "Stop record: click the \"record\" button again."
msgstr "Aturar l'enregistrament: torneu a fer clic en el botó «enregistra»."

#: ../../importing_and_assets_management/capturing.rst:116
msgid "The recorded clip will be added in the project bin."
msgstr "El clip enregistrat s'afegirà a la safata del projecte."

#: ../../importing_and_assets_management/capturing.rst:119
msgid "Settings can be adjusted in :ref:`configure_kdenlive`"
msgstr "Les opcions es poden ajustar a :ref:`configure_kdenlive`"

# skip-rule: t-acc_obe
#: ../../importing_and_assets_management/capturing.rst:122
msgid ""
"To check on your linux distro, type ``ffmpeg -version`` in a terminal and "
"look for ``--enable-x11grab`` in the reported configuration info.  [1]_"
msgstr ""
"Per a comprovar la vostra distribució de Linux, escriviu ``ffmpeg -version`` "
"en un terminal i cerqueu ``--enable-x11grab`` en la informació de "
"configuració llistada. [1]_"

#: ../../importing_and_assets_management/capturing.rst:125
msgid ""
"If you are capturing the screen and using the X246 with audio settings and "
"you get a crash as shown in the screen shot…"
msgstr ""
"Si esteu capturant la pantalla i utilitzant el X246 amb la configuració "
"d'àudio i obteniu una fallada com es mostra a la captura de pantalla…"

#: ../../importing_and_assets_management/capturing.rst:131
msgid ""
"…then consider creating a profile for audio capture where ``-acodec "
"pcm_s16le``  is replaced by ``-acodec libvorbis -b 320k``. See :ref:"
"`configure_kdenlive`."
msgstr ""
"…llavors considereu crear un perfil per a la captura d'àudio on``-acodec "
"pcm_s16le`` és substituït per ``-acodec libvorbis -b 320k``. Vegeu :ref:"
"`configure_kdenlive`."

#: ../../importing_and_assets_management/capturing.rst:135
msgid "Blackmagic"
msgstr "Blackmagic"

# skip-rule: t-acc_obe,da-se
#: ../../importing_and_assets_management/capturing.rst:139
msgid ""
"This is for capturing from Blackmagics `decklink <http://www.blackmagic-"
"design.com/uk/products/decklink/>`_ video capture cards (AFAIK). Not sure "
"how stable this code is at the moment. Mentioned in legacy Mantis bug "
"tracker ID 2130."
msgstr ""
"Això és per a capturar des de targetes de captura de vídeo `DeckLink <http://"
"www.blackmagic-design.com/uk/products/decklink/>`_ de Blackmagic (AFAIK). No "
"sé com és d'estable aquest codi en aquest moment. S'ha esmentat en el "
"seguiment d'errors antic de Mantis ID 2130."

#: ../../importing_and_assets_management/capturing.rst:143
msgid "Footnotes"
msgstr "Notes al peu"

# skip-rule: t-acc_obe
#: ../../importing_and_assets_management/capturing.rst:147
msgid ""
"There are now two branches of *ffmpeg*: a *Libav* branch and an ffmpeg.org "
"branch. The *ffmpeg* version from the latter branch reports the "
"configuration when you run with ``ffmpeg -version``. The *Libav* version "
"does not. So this method to check for the ``--enable-x11grab`` does not work "
"if you have the *Libav* version of *ffmpeg*."
msgstr ""
"Ara hi ha dues branques del *ffmpeg*: una branca *Libav* i una branca a "
"*ffmpeg.org*. La versió *ffmpeg* de l'última branca informa de la "
"configuració quan s'executa amb ``ffmpeg -version``. La versió *Libav* no. "
"Per tant, aquest mètode per a comprovar el ``--enable-x11grab`` no funciona "
"si teniu la versió *Libav* de *ffmpeg*."
