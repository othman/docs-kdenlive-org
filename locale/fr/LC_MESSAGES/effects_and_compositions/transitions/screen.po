# SPDX-FileCopyrightText: 2021, 2022, 2023 Xavier Besnard <xavier.besnard@kde.org>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-12-10 12:03+0100\n"
"Last-Translator: Xavier BESNARD <xavier.besnard@neuf.fr>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.3\n"

#: ../../effects_and_compositions/transitions/screen.rst:13
msgid "Transitions - Screen"
msgstr "Transitions - Écran"

#: ../../effects_and_compositions/transitions/screen.rst:17
msgid ""
"This is very much like :doc:`/effects_and_compositions/video_effects/"
"alpha_mask_keying/chroma_key` but it works with footage filmed against a "
"black background."
msgstr ""
"Cette fonction est très proche de :doc:`/effects_and_compositions/"
"video_effects/alpha_mask_keying/chroma_key`. Mais, elle fonctionne avec des "
"séquences filmées sur un fond noir."

#: ../../effects_and_compositions/transitions/screen.rst:19
msgid "Timeline showing how to apply the \"Screen\" transition."
msgstr ""
"Frise chronologique montrant comment appliquer la transition « Écran »."

#: ../../effects_and_compositions/transitions/screen.rst:25
msgid ""
"This video composites a video of fire filmed on a black background into "
"another bit of footage using the *Screen* transition."
msgstr ""
"Cette vidéo compose une vidéo de feu filmée sur un fond noir avec une autre "
"séquence en utilisant la transition *Écran*."

#: ../../effects_and_compositions/transitions/screen.rst:27
msgid "https://youtu.be/GkFdHcf9jbY"
msgstr "https://youtu.be/GkFdHcf9jbY"

#~ msgid "Contents"
#~ msgstr "Contenu"
