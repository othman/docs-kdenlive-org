# SPDX-FileCopyrightText: 2021, 2022, 2023 Xavier Besnard <xavier.besnard@kde.org>
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-11-27 23:06+0100\n"
"Last-Translator: Xavier BESNARD <xavier.besnard@neuf.fr>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 23.08.3\n"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:16
msgid "Backup"
msgstr "Sauvegarder"

#: ../../importing_and_assets_management/projects_and_files/backup.rst:24
msgid ""
"The Backup widget, found in :menuselection:`Project --> Open Backup File` "
"allows you to restore a previous version of your project file."
msgstr ""
"Le composant graphique « Sauvegarde », trouvé dans :menuselection:`Projet / "
"Ouvrir un fichier de sauvegarde` vous permet de restaurer une version "
"précédente de votre fichier de projet."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:27
msgid ""
"In case something went wrong (corrupted project file, unwanted change, ...), "
"you can now restore a previous version of the file using this feature. Just "
"select the version you want and click :menuselection:`Open`."
msgstr ""
"Si quelque chose s'est mal passé (fichier de projet corrompu, modification "
"non désirée, etc.), vous pouvez maintenant restaurer une version précédente "
"du fichier en utilisant cette fonction. Il suffit de sélectionner la version "
"souhaitée et de cliquer sur :menuselection:`Ouvrir`."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:29
msgid ""
"The backup files are automatically created each time you save your project. "
"This means that if you save your project every hour, the backup widget will "
"show you a list of all the saved files, with a small image of the timeline "
"at the time you saved the project."
msgstr ""
"Les fichiers de sauvegarde sont automatiquement créés chaque fois que vous "
"enregistrez votre projet. Cela signifie que si vous enregistrez votre projet "
"toutes les heures, le composant graphique de sauvegarde vous affichera une "
"liste de tous les fichiers enregistrés, avec une petite image de la frise "
"chronologique au moment où vous avez enregistré le projet."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:32
msgid ""
"**Kdenlive** keeps up to 20 versions of your project file in the last hour, "
"20 versions from the current day, 20 versions in the last 7 days and 20 "
"older versions, which should be sufficient to recover from any problem."
msgstr ""
"**Kdenlive** conserve jusqu'à 20 versions de votre fichier de projet durant "
"la dernière heure, 20 versions durant la journée en cours, 20 versions "
"durant 7 derniers jours et 20 versions plus anciennes, ce qui devrait être "
"suffisant pour faire face à tout problème."

#: ../../importing_and_assets_management/projects_and_files/backup.rst:34
msgid ""
"More details how to get backups when the project file version is upgraded "
"see :ref:`project`."
msgstr ""
"Plus de détails sur la façon d'obtenir des sauvegardes lorsque la version du "
"fichier de projet est mise à niveau. Veuillez consulter : :ref:`project`."

#~ msgid "Contents"
#~ msgstr "Contenu"
