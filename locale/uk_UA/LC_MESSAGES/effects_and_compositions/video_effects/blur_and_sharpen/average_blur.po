# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-12-12 17:40+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**Status**"
msgstr "**Стан**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**Keyframes**"
msgstr "**Ключові кадри**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**Source library**"
msgstr "**Початкова бібліотека**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**Source filter**"
msgstr "**Початковий фільтр**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**Available**"
msgstr "**Доступність**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**On Master only**"
msgstr "**Лише на основі**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:0
msgid "**Known bugs**"
msgstr "**Відомі вади**"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:1
msgid "Kdenlive Video Effects - Average Blur"
msgstr "Відеоефекти Kdenlive — середнє розмивання"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, blur and sharpen, average blur "
msgstr ""
"KDE, Kdenlive, відеоредактор, довідка, вивчення, просте, ефекти, фільтр, "
"відеоефекти, розмивання і збільшення різкості, середнє розмивання"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:1
msgid ""
"- Roger (https://userbase.kde.org/User:Roger) - Bernd Jordan (https://"
"discuss.kde.org/u/berndmj) "
msgstr ""
"- Roger (https://userbase.kde.org/User:Roger) - Bernd Jordan (https://"
"discuss.kde.org/u/berndmj)"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:1
msgid "Creative Commons License SA 4.0"
msgstr "Creative Commons License SA 4.0"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:13
msgid "Average Blur"
msgstr "Середнє розмивання"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:24
msgid "Maintained"
msgstr "Супровід"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:26
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:34
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:36
msgid "No"
msgstr "Ні"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:28
msgid "avfilter"
msgstr "avfilter"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:30
msgid "avgblur"
msgstr "avgblur"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:32
msgid "|linux| |appimage| |windows| |apple|"
msgstr "|linux| |appimage| |windows| |apple|"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:42
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:56
msgid "Description"
msgstr "Опис"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:43
msgid ""
"Blurs the clip based on the settings for :guilabel:`X size` and :guilabel:`Y "
"size`. By default, all :term:`planes<plane>` will be affected. By setting "
"the blur effect to different planes (e.g. red, green or blue) interesting "
"artistic effects can be achieved."
msgstr ""
"Розмиває зображення на основі параметрів :guilabel:`Розмір за X` і :guilabel:"
"`Розмір за Y`. Типово, це стосуватиметься усіх :term:`площин <plane>`, але "
"встановленням розмивання для різних площин (наприклад червоної, зеленої або "
"синьої) надає змогу створити цікаві художні ефекти."

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:47
msgid "Parameters"
msgstr "Параметри"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:54
msgid "Parameter"
msgstr "Параметр"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:55
msgid "Value"
msgstr "Значення"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:57
msgid "X size"
msgstr "Розмір за X"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:58
#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:61
msgid "Integer"
msgstr "Ціле"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:59
msgid "Amount of horizontal blur"
msgstr "Величина горизонтального розмивання"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:60
msgid "Y size"
msgstr "Розмір за Y"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:62
msgid "Amount of vertical blur"
msgstr "Величина вертикального розмивання"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:63
msgid "Planes"
msgstr "%1 площин"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:64
msgid "Selection"
msgstr "Вибір"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:65
msgid "Sets the color space plane the effect is applied to"
msgstr "Встановлює площину у просторі кольорів, до якої застосовано ефект"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:67
msgid "The following selection items are available:"
msgstr "Доступними є такі пункти вибору:"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:69
msgid ":guilabel:`Planes`"
msgstr ":guilabel:`Площини`"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:76
msgid "Alpha"
msgstr "Альфа"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:77
msgid "Alpha channel"
msgstr "Канал прозорості"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:78
msgid "Y"
msgstr "Y"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:79
msgid "Luminance"
msgstr "Яскравість"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:80
msgid "U"
msgstr "U"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:81
msgid "Chroma (U plane)"
msgstr "Інтенсивність (U-площина)"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:82
msgid "V"
msgstr "V"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:83
msgid "Chroma (V plane)"
msgstr "Інтенсивність (V-площина)"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:84
msgid "Red"
msgstr "Червоний"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:85
msgid "Red channel"
msgstr "Канал червоного"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:86
msgid "Green"
msgstr "Зелений"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:87
msgid "Green channel"
msgstr "Канал зеленого"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:88
msgid "Blue"
msgstr "Синій"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:89
msgid "Blue channel"
msgstr "Канал синього"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:90
msgid "All"
msgstr "Усі"

#: ../../effects_and_compositions/video_effects/blur_and_sharpen/average_blur.rst:91
msgid "All planes will be affected by the blur amount (default)"
msgstr "Величину розмивання буде застосовано до усіх площин (типовий варіант)"

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using average blur effect"
#~ msgstr ""
#~ "Ваші перші кроки у відеоредакторі Kdenlive, використовуємо ефект "
#~ "середнього розмивання"

#~ msgid "This effect does not have keyframes."
#~ msgstr "У цьому ефекті не передбачено прив'язування до ключових кадрів."

#~ msgid "Average Blur effect"
#~ msgstr "Ефекти середнього розмивання"
