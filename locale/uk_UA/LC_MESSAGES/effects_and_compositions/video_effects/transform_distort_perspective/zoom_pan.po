# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-31 00:36+0000\n"
"PO-Revision-Date: 2023-12-11 22:46+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**Status**"
msgstr "**Стан**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**Keyframes**"
msgstr "**Ключові кадри**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**Source library**"
msgstr "**Початкова бібліотека**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**Source filter**"
msgstr "**Початковий фільтр**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**Available**"
msgstr "**Доступність**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**On Master only**"
msgstr "**Лише на основі**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:0
msgid "**Known bugs**"
msgstr "**Відомі вади**"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:1
msgid "Kdenlive Video Effects - Zoom Pan"
msgstr "Відеоефекти Kdenlive — панорамування із масштабуванням"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, transform, distort, perspective, zoom pan"
msgstr ""
"KDE, Kdenlive, відеоредактор, довідка, вивчення, просте, ефекти, фільтр, "
"відеоефекти, перетворення, викривлення, перспектива, панорамування із "
"масштабуванням"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:19
msgid "Zoom Pan"
msgstr "Панорамування із масштабуванням"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:30
msgid "Maintained"
msgstr "Супровід"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:32
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:40
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:42
msgid "No"
msgstr "Ні"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:34
msgid "avfilter"
msgstr "avfilter"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:36
msgid "zoompan"
msgstr "zoompan"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:38
msgid "|linux| |appimage| |windows| |apple|"
msgstr "|linux| |appimage| |windows| |apple|"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:48
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:62
msgid "Description"
msgstr "Опис"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:49
msgid ""
"This effect/filter applies a zoom and pan effect (similar to the Ken Burns "
"effect\\ [1]_)."
msgstr ""
"Цей ефект або фільтр застосовує ефект масштабування і панорамування "
"(подібний до ефекту Кена Бернса\\ [1]_)."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:53
msgid "Parameters"
msgstr "Параметри"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:60
msgid "Parameter"
msgstr "Параметр"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:61
msgid "Value"
msgstr "Значення"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:63
msgid "Zoom"
msgstr "Масштаб"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:64
#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:67
msgid "Integer"
msgstr "Ціле"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:65
msgid ""
"Set the zoom factor. The clip is zoomed with the top left corner as the "
"origin."
msgstr ""
"Встановлює коефіцієнт масштабування. Масштабування зображення кліпу "
"відбувається з використанням верхнього лівого кута як початку координат."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:66
msgid "X / Y"
msgstr "X / Y"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:68
msgid "Move the clip along the X / Y axis"
msgstr "Пересунути кліп уздовж вісі X / Y"

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:72
msgid ""
"As of this writing and with version 23.04 the effect seems to be missing "
"several parameters in order to make it useful as a zoom and pan effect, like "
"the |ken_burns|. A bug report has been created. Until this is fixed use the :"
"doc:`/effects_and_compositions/video_effects/transform_distort_perspective/"
"transform` effect."
msgstr ""
"На час написання цього підручника і версії 23.04, здається, в ефекті не було "
"надано доступ до декількох параметрів, які могли б зробити його корисним, як "
"ефект масштабування і панорамування, подібно до ефекту |ken_burns|. Було "
"створено звіт щодо вади. До виправлення вади рекомендуємо користуватися "
"ефектом :doc:`/effects_and_compositions/video_effects/"
"transform_distort_perspective/transform`."

#: ../../effects_and_compositions/video_effects/transform_distort_perspective/zoom_pan.rst:77
msgid "For more details refer to the |ken_burns| article in Wikipedia"
msgstr "Щоб дізнатися більше, зверніться до статті |ken_burns| у Вікіпедії"

#~ msgid "**Notes**"
#~ msgstr "**Нотатки**"

#~ msgid ""
#~ "Do your first steps with Kdenlive video editor, using zoom pan effect"
#~ msgstr ""
#~ "Ваші перші кроки у відеоредакторі Kdenlive, використовуємо ефект "
#~ "панорамування і масштабування"

#~ msgid "The effect does not have keyframes."
#~ msgstr "У цьому ефекті не передбачено прив'язування до ключових кадрів."

#~ msgid "Zoom Pan effect"
#~ msgstr "Ефект панорамування із масштабуванням"
