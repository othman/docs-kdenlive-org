# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2021-11-26 23:01+0200\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../cutting_and_assembling/grouping.rst:16
msgid "Grouping"
msgstr "Групування"

#: ../../cutting_and_assembling/grouping.rst:19
msgid ""
"Grouping clips allows you to lock clips together so that you can move them "
"as a group and still retain their positions relative to each element in the "
"group."
msgstr ""
"Групування кліпів надає вам змогу з'єднувати кліпи так, щоб їх можна було "
"пересувати як групу зі збереженням взаємного розташування кожного з "
"елементів групи."

#: ../../cutting_and_assembling/grouping.rst:24
msgid "How to Group Clips"
msgstr "Групування кліпів"

#: ../../cutting_and_assembling/grouping.rst:26
msgid ""
"You can select multiple clips in preparation for grouping them by holding "
"shift and clicking the mouse and dragging in the timeline."
msgstr ""
"Ви можете позначити декілька кліпів у приготуванні їх до групування, "
"утримуючи натиснутою клавішу :kbd:`Shift` і одночасно клацаючи лівою кнопкою "
"миші або затисканням лівої кнопки миші із наступним перетягуванням "
"вказівника монтажним столом."

#: ../../cutting_and_assembling/grouping.rst:34
msgid ""
"To group the selected clips select :menuselection:`Timeline --> Group Clips` "
"or right-click the selected clips and choose :menuselection:`Group Clips`."
msgstr ""
"Щоб згрупувати позначені кліпи, скористайтеся пунктом меню :menuselection:"
"`Монтажний стіл --> Згрупувати кліпи` або клацніть правою кнопкою миші на "
"позначених кліпах і виберіть у контекстному меню пункт :menuselection:"
"`Згрупувати кліпи`."

#: ../../cutting_and_assembling/grouping.rst:39
msgid "Cutting Grouped Clips"
msgstr "Розрізання згрупованих кліпів"

#: ../../cutting_and_assembling/grouping.rst:41
msgid ""
"Grouping is also useful if you have separate audio and video tracks and need "
"to cut and splice both tracks at exactly the same point (e.g. for audio sync "
"reasons)."
msgstr ""
"Групування також корисне, якщо вам потрібно відокремити звукові доріжки і "
"відеодоріжки і розрізати обидві доріжки у тій самій точці (наприклад, з "
"метою збереження синхронізації відео і звуку)."

#: ../../cutting_and_assembling/grouping.rst:44
msgid ""
"If you cut the video clip using the :ref:`editing` when there is an audio "
"clip grouped to it, then **Kdenlive** cuts the audio clip at the same point "
"automatically."
msgstr ""
"Якщо ви розріжете відеокліп за допомогою :ref:`інструмента леза <editing>`, "
"і з цим кліпом згрупований звуковий кліп, **Kdenlive** розріже звуковий кліп "
"у тих самих місцях автоматично."

#: ../../cutting_and_assembling/grouping.rst:65
msgid "Removing Clip Grouping"
msgstr "Вилучення групування кліпів"

#: ../../cutting_and_assembling/grouping.rst:67
msgid ""
"To remove the grouping on clips, select the group of clips and choose :"
"menuselection:`Timeline --> Ungroup Clips`."
msgstr ""
"Щоб скасувати групування кліпів, позначте групу кліпів і скористайтеся "
"пунктом меню :menuselection:`Монтажний стіл --> Розгрупувати кліпи`."

#: ../../cutting_and_assembling/grouping.rst:71
msgid "FAQ"
msgstr "Поширені питання"

#: ../../cutting_and_assembling/grouping.rst:73
msgid "Q: How to delete sound track only?"
msgstr "Питання: Як вилучити лише звукову доріжку?"

#: ../../cutting_and_assembling/grouping.rst:75
msgid ""
"A: Right-click on the clip and choose :menuselection:`Split Audio`. The "
"audio will move to an audio track but be grouped with the video track."
msgstr ""
"Відповідь: Клацніть правою кнопкою на кліпі і виберіть у контекстному меню "
"пункт :menuselection:`Відділити звук`. Звукові дані буде пересунуто на "
"звукову доріжку і згруповано зі відеодоріжкою."

#: ../../cutting_and_assembling/grouping.rst:81
msgid "Right-click again and choose :menuselection:`Ungroup Clips`."
msgstr ""
"Клацніть правою кнопкою миші ще раз і виберіть у контекстному меню пункт :"
"menuselection:`Розгрупувати кліпи`."

#: ../../cutting_and_assembling/grouping.rst:83
msgid "Then you can delete just the audio track."
msgstr "Далі ви можете вилучити лише звукову доріжку."

#: ../../cutting_and_assembling/grouping.rst:85
msgid ""
"Alternatively you can keep the audio in the clip and use the :menuselection:"
"`Audio Correction --> Mute` effect to just mute the soundtrack on the clip."
msgstr ""
"Крім того, ви можете зберегти звукові дані у кліпі і скористатися ефектом :"
"menuselection:`Виправлення звуку --> Вимкнути звук`, щоб просто вимкнути "
"звукову доріжку у кліпі."

#: ../../cutting_and_assembling/grouping.rst:87
msgid ""
"Yet another method is to select :menuselection:`Video only` from the :ref:"
"`clip_menu`."
msgstr ""
"Ще одним способом є вибір пункту :menuselection:`Лише відео` у :ref:`меню "
"«Кліп» <clip_menu>`."

#~ msgid "Contents"
#~ msgstr "Зміст"
