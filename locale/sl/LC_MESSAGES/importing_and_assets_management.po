# Translation of kdenlive Manual to Slovenian
#
# Copyright (C) 2022 This_file_is_part_of_KDE
# This file is distributed under the same license as the kdenlive package.
#
#
# Martin Srebotnjak <miles@filmsi.net>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: kdenlive ref manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2022-08-14 00:24+0200\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"X-Generator: Poedit 3.1.1\n"
"X-Poedit-SourceCharset: ISO-8859-1\n"

#: ../../importing_and_assets_management.rst:1
msgid "Load files and start video editing with Kdenlive"
msgstr "Naložite datoteke in začnite montirati videoposnetke s Kdenlive"

#: ../../importing_and_assets_management.rst:1
msgid ""
"KDE, Kdenlive, file loading, start video editing, first steps, "
"documentation, user manual, video editor, open source, free, learn, easy"
msgstr ""
"KDE, Kdenlive, nalaganje datotek, začetek urejanja videoposnetkov, prvi "
"koraki, dokumentacija, uporabniški priročnik, montažni program, program za "
"montažo, video urejevalnik, odprta koda, brezplačno, prosto, učenje, "
"enostavno"

#: ../../importing_and_assets_management.rst:15
msgid "Importing and assets management"
msgstr "Uvažanje in upravljanje z vsebinami"

#~ msgid "Contents:"
#~ msgstr "Vsebina:"
