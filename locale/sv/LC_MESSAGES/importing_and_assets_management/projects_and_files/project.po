# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-08-19 08:51+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: ../../importing_and_assets_management/projects_and_files/project.rst:18
msgid "Project File Details"
msgstr "Information om projektfil"

#: ../../importing_and_assets_management/projects_and_files/project.rst:21
msgid ""
"**Kdenlive** projects consist in a singe :file:`.kdenlive` file (in XML "
"format), gathering :"
msgstr ""
"Ett projekt i **Kdenlive** består av en enda :file:`.kdenlive`-fil (med XML-"
"format), som samlar:"

#: ../../importing_and_assets_management/projects_and_files/project.rst:24
msgid "target video and audio properties"
msgstr "video och ljudegenskaper för resultatet"

#: ../../importing_and_assets_management/projects_and_files/project.rst:26
msgid ""
"references to all the source materials (and to their lighter *proxies* work "
"copies)"
msgstr ""
"referenser till allt källmaterial (och till deras mindre *ersättningar* i "
"form av arbetskopior)"

#: ../../importing_and_assets_management/projects_and_files/project.rst:28
msgid ""
"clips arrangement on the timeline, with effects applied, and everything to "
"get the final result"
msgstr ""
"klippens arrangemang på tidslinjen, med införda effekter, och allting för "
"att få slutresultatet"

#: ../../importing_and_assets_management/projects_and_files/project.rst:31
msgid ""
"Project files are associated with a working directory, in which **Kdenlive** "
"will generate *proxies* and *thumbs*, so that an overview of your media "
"always shows up quickly (if you move your project file, you should declare "
"the directory change in the project properties)."
msgstr ""
"Projektfiler hör ihop med en arbetskopia, där **Kdenlive** genererar "
"**ersättningar** och **miniatyrbilder**, så att en översikt av media alltid "
"visas snabbt (om man flyttar projektfilen, måste man deklarera "
"katalogändringen i projektegenskaperna)."

#: ../../importing_and_assets_management/projects_and_files/project.rst:33
msgid ""
"When Kdenlive opens a project file that upgrades the document version, like "
"with 20.08.0 or 23.04.0, it automatically creates a backup copy of the "
"original project file. When a project was created with an older document "
"version, like Kdenlive < 20.08.0 or < 23.04.0 and you opened it, look into "
"the project folder. You should have a file called `myproject_backup."
"kdenlive`. This is a copy of the original project file before the upgrade "
"and you should be able to open it correctly either with Kdenlive 20.04.x or "
"22.12.x or the newly release 23.04.0 version."
msgstr ""
"När Kdenlive öppnar en projektfil som uppgraderar dokumentversionen, som med "
"20.08.0 eller 23.04.0, skapar den automatiskt en säkerhetskopia av den "
"ursprungliga projektfilen. När ett projekt skapades med en äldre "
"dokumentversion, som Kdenlive tidigare än 20.08.0 eller 23.04.0 och den "
"öppnas, titta in i projektmappen. Det bör finnas en fil som heter "
"`mitt_projekt_backup.kdenlive`. Det är en kopia av den ursprungliga "
"projektfilen före uppgraderingen och den bör kunna öppnas korrekt antingen "
"med Kdenlive 20.04.x eller 22.12.x eller den nyligen utgivna versionen "
"23.04.0."

#: ../../importing_and_assets_management/projects_and_files/project.rst:35
msgid ""
"Otherwise, Kdenlive also keeps a copy of all saved versions of your project "
"files. Go to :menuselection:`Project --> Open Backup File` and you should "
"see a list of the archived versions. More details see :ref:`backup`."
msgstr ""
"Annars behåller Kdenlive också en kopia av alla sparade versioner av "
"projektfilerna. Gå till :menuselection:`Projekt --> Öppna säkerhetskopia` så "
"ska en lista över de arkiverade versionerna visas. För mer information, se :"
"ref:`backup`."

#: ../../importing_and_assets_management/projects_and_files/project.rst:41
msgid ""
"A major refactoring of the project file fixes a long standing issue with the "
"decimal separator (comma/point) conflict causing many crashes."
msgstr ""
"En större omstrukturering av projektfilen rättar ett långvarigt problem med "
"konflikt för decimaltecken (kommatecken eller punkt) som orsakat många "
"krascher."

#: ../../importing_and_assets_management/projects_and_files/project.rst:45
msgid ""
"Projects created with 20.08 forward are not backwards compatible, that is, "
"you won’t be able to open your :file:`.kdenlive` project files with older "
"versions."
msgstr ""
"Projekt skapade med 20.08 eller senare är inte bakåtkompatibla, det vill "
"säga, man kan inte öppna :file:`.kdenlive` projektfiler med äldre versioner."

#: ../../importing_and_assets_management/projects_and_files/project.rst:49
msgid ""
"With introducing sequences the project file version is 1.1 (from 1.04 -> "
"1.1) and is not backward compatible. Once opened in 23.04 you cannot open "
"the project file in older versions."
msgstr ""
"Efter sekvenser introducerades är projektfilens version 1.1 (från 1.04 -> "
"1.1) och är inte bakåtkompatibel. När den väl öppnats av 23.04 kan "
"projektfilen inte öppnas i äldre versioner."

#: ../../importing_and_assets_management/projects_and_files/project.rst:53
msgid ""
"Projects created with 23.04 forward are not backwards compatible, that is, "
"you won’t be able to open your :file:`.kdenlive` project files with older "
"versions."
msgstr ""
"Projekt skapade med 23.04 eller senare är inte bakåtkompatibla, det vill "
"säga, man kan inte öppna :file:`.kdenlive` projektfiler med äldre versioner."

#~ msgid "Contents"
#~ msgstr "Innehåll"
