# German translations for Kdenlive Manual package.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Frederik Schwarzer <schwarzer@kde.org>, 2022.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2022-06-08 01:09+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.2\n"

#: ../../effects_and_compositions/audio_effects/audio_correction/volume_keyframable.rst:11
msgid "Volume (keyframable)"
msgstr ""

#: ../../effects_and_compositions/audio_effects/audio_correction/volume_keyframable.rst:14
msgid ""
"This is an audio effect to change the volume of a clip using keyframes "
"(change of effect over time.) Volume (Keyframable) uses decibels as opposed "
"to `Gain <https://userbase.kde.org/Kdenlive/Manual/Effects/Audio_Correction/"
"Gain>`_."
msgstr ""

#~ msgid "Contents"
#~ msgstr "Inhalt"
