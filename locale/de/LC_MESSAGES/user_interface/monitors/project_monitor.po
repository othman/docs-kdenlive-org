# German translations for Kdenlive Manual package
# German translation for Kdenlive Manual.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2023-09-11 09:05+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../user_interface/monitors/project_monitor.rst:1
msgid "Kdenlive's User Interface - Project Monitor"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:1
msgid ""
"KDE, Kdenlive, clip, project, monitor, project monitor, overlay, resizing, "
"zoombar, preview, toolbar, documentation, user manual, video editor, open "
"source, free, learn, easy"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:17
msgid "Project Monitor"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:25
msgid "Project Monitor elements"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:27
msgid ""
"The Project Monitor displays your project's :term:`timeline` - i.e. the "
"edited version of your video. It has the same functions and options as the :"
"ref:`ui-monitors_clip_monitor` except for the |edit-mode| icon which "
"switches :term:`Edit Mode` on or off."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:29
msgid ""
"For more details about the icons in the monitor toolbar refer to :ref:`this "
"section <ui_elements-monitor_icons>` of the manual."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:35
msgid "Project Monitor Controls"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:37
msgid ""
"The position caret. Shows the current location in the project relative to "
"the whole project. You can click and drag this to move the position in the "
"project."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:39
msgid ""
"The timecode control. You can type a timecode here and press :kbd:`Enter` to "
"bring the playhead to an exact location."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:41
msgid ""
"Timecode control arrows. You can move the Project Monitor one frame at a "
"time with these. Alternatively, you can use :kbd:`left` and :kbd:`right` "
"(keyboard arrow keys)."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:47
msgid "Hamburger Menu"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:55
msgid "Project Monitor options"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:57
msgid ""
"**Volume** - Set the Project Monitor playback volume. This does not affect "
"the master volume setting."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:59
msgid ""
"**Go to Guide** - If :term:`guides<guide>` are defined the list of guides is "
"displayed and selecting one positions the :term:`playhead` at the guide's "
"timecode."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:61
msgid ""
"**Force Monitor Size** - You can force the Project Monitor to display the "
"video at 100% (original size), 50% or adjust it to the Project Monitor "
"widget's size."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:63
msgid ""
"**Set current image as thumbnail** - Takes the a snapshot of the current "
"frame and uses that as the thumbnail for the clip."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:65
msgid ""
"**Audio Scrubbing** - If enabled plays the audio track while scrubbing "
"through the timeline. You may want to turn this off to improve scrubbing "
"performance or if the noise is distracting."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:67
msgid ""
"**Show Audio Levels** - If enabled shows the frequency curve of the master "
"audio in the Project Monitor."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:75
msgid "Creating Zones"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:83
msgid "Project Monitor zone"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:85
msgid ""
"You can use the :kbd:`I` and :kbd:`O` keys or the |zone-in| and |zone-out| "
"icons to create a zone in the Project Monitor the same way you make zones in "
"the Clip Monitor with the notable exception that the zone defined in the "
"Project Monitor also sets the zone in the :term:`Timeline`. The zone will be "
"indicated by a colored bar both on the :term:`timeline` and underneath the "
"Project Monitor."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:89
msgid ""
"You can have Kdenlive only render the selected zone - see :ref:`rendering-"
"selected_zone`."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:95
msgid "Right-click Menu"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:103
msgid "Project Monitor right-click menu options"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:105
msgid ""
"These are the menu items that are available when you right-click in the "
"Project Monitor. These actions affect the clip that is currently selected in "
"the :ref:`timeline`. Similar menu items are available from a right-click "
"menu in the :ref:`ui-monitors_clip_monitor`. However, the clip monitor menu "
"items affect the currently selected clip in the :ref:`project bin "
"<project_tree>`."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:117
msgid "Item"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:118
msgid "Shortcut"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:119
msgid "Description"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:120
msgid "Play"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:122
msgid "Plays the :term:`clip` currently selected in the :term:`project bin`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:123
msgid "Play Zone"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:124
msgid ":kbd:`Ctrl+Space`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:125
msgid "Plays the current :term:`zone` and stops"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:126
msgid "Loop Zone"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:127
msgid ":kbd:`Ctrl+Shift+Space`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:128
msgid "Plays the current :term:`zone` in a continuous loop"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:129
msgid "Loop Selected Clip"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:131
msgid "Plays the currently selected :term:`clip` in a continuous loop"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:132
msgid "Go to Project Start"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:133
msgid ":kbd:`Ctrl+Home`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:134
msgid "Goes to the beginning of the clip"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:135
msgid "Go to Previous Guide"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:136
msgid ":kbd:`Ctrl+Left`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:137
msgid "Goes to the previous :term:`guide`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:138
msgid "Go to Previous Snap Point"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:139
msgid ":kbd:`Alt+Left`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:140
msgid "Moves the :term:`playhead` to the previous :term:`snap point`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:141
msgid "Go to Zone Start"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:142
msgid ":kbd:`Shift+I`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:143
msgid "Goes to the start of the :term:`zone`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:144
msgid "Go to Clip Start"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:145
msgid ":kbd:`Home`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:146
msgid "Moves the clip playhead to the beginning of the clip"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:147
msgid "Go to Clip End"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:148
msgid ":kbd:`End`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:149
msgid "Moves the clip playhead to the end of the clip"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:150
msgid "Go to Zone End"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:151
msgid ":kbd:`Shift+O`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:152
msgid "Goes to the end of the :term:`zone`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:153
msgid "Go to Next Snap Point"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:154
msgid ":kbd:`Alt+Right`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:155
msgid "Moves the :term:`playhead` to the next :term:`snap point`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:156
msgid "Go to Next Guide"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:157
msgid ":kbd:`Ctrl+Right`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:158
msgid "Goes to the next :term:`guide`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:159
msgid "Go to Project End"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:160
msgid ":kbd:`Ctrl+End`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:161
msgid "Goes the end of the clip"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:162
msgid "Go to Guide..."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:164
msgid ""
"The menu item pops out a list of existing :term:`guides<guide>` to select "
"from. When one is selected the playhead moves to that guide."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:165
msgid "Extract Frame"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:167
msgid ""
"Opens the **Save Image** dialog window to save the current frame as an image "
"file (default is :file:`.png`) to your file system"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:168
msgid "Extract Frame to Project"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:170
msgid ""
"Same as :guilabel:`Extract Frame` but in addition the image file is brought "
"into the project bin"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:177
msgid "Save Image dialog for extracting frames"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:184
msgid "Add Project Note"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:186
msgid ""
"Opens the :ref:`Project Notes <notes>` widget and adds a hyperlink to the "
"current frame in the clip. You can enter more text to describe the scene."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:187
msgid "Set Zone In"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:188
msgid ":kbd:`I`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:189
msgid "Sets the :term:`in-point` for the :term:`zone`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:190
msgid "Set Zone Out"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:191
msgid ":kbd:`O`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:192
msgid "Sets the :term:`out-point` for the :term:`zone`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:193
msgid "Set current image as thumbnail"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:195
msgid "Uses the current frame as the thumbnail for the clip in the project bin"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:196
#: ../../user_interface/monitors/project_monitor.rst:219
msgid "Multitrack View"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:197
msgid ":kbd:`F12`"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:198
msgid ""
"Switches the project monitor area to the :ref:`ui-"
"monitors_pm_multitrack_view`. All active tracks will be displayed. For more "
"details about how to use this in Editing refer to the :ref:`multicam_tool` "
"chapter."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:199
msgid "Current Monitor Overlay"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:201
msgid "Opens a fly-out for the various available monitor overlays"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:202
msgid "Monitor Info Overlay"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:204
msgid "Switches all monitor overlays on or off"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:205
msgid "Monitor Overlay Timecode"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:207
msgid "Switches the display of the timecode on or off"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:208
msgid "Monitor Overlay Playback FPS"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:210
msgid "Switches the display of the frame-per-seconds (:term:`fps`) on or off"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:211
msgid "Monitor Overlay Markers"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:213
msgid "Switches the display of the marker lines and thumbnails on or off"
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:227
msgid ""
"Selecting this allows you to view all the video tracks at once in a split "
"screen in the project monitor. Kdenlive starts with video track #1 in the "
"top left corner and displays the other tracks sequentially. Hidden tracks "
"are not displayed."
msgstr ""

#: ../../user_interface/monitors/project_monitor.rst:229
msgid ""
"For more details about how to use this in Editing refer to the :ref:"
"`multicam_tool` chapter."
msgstr ""
