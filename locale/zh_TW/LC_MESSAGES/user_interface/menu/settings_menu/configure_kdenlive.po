# Chinese translations for Kdenlive Manual package
# Kdenlive Manual 套件的正體中文翻譯.
# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Automatically generated, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2022-04-16 00:39+0000\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:1
msgid "Configure settings in Kdenlive video editor"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:1
msgid ""
"KDE, Kdenlive, setting, file type, editing, timeline, documentation, user "
"manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:24
msgid "Configure Kdenlive"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:27
msgid "Following settings applies when you start a project with :ref:`new`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:32
msgid "Misc"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:40
msgid "**Project file**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:42
msgid ":guilabel:`Open the last project on startup`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:44
msgid ":guilabel:`Activate Crash recovery` (:ref:`auto_save`)"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:46
msgid "**Clip import**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:48
msgid ":guilabel:`Check if the first added clip matches the project profile`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:50
msgid ""
":guilabel:`Automatically import all streams in multi stream clips` Here you "
"set if all audio streams are loaded/imported."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:52
msgid ":guilabel:`Automatically import image sequences`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:54
msgid ":guilabel:`Get clip metadata with exiftool`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:56
msgid ":guilabel:`Get clip metadata created by Magic Lantern`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:58
msgid ""
":guilabel:`Ignore subfolder structure on import (import all files into "
"toplevel folder)`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:60
#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:66
msgid "**-------**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:62
msgid ":guilabel:`Disable parameters when the effect is disabled`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:64
msgid ":guilabel:`Tab position` Set the tab position when a window is open."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:68
msgid ""
":guilabel:`Preferred track compositing composition:` Select :menuselection:"
"`auto`, :menuselection:`frei0r.cairoblend`, :menuselection:`qtblend`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:72
msgid ""
"Qtblend brings back a much better playback performance when there is no "
"compositing. When there is a compositing, performance is slightly worse than "
"frei0r.cairoblend (Kdenlive lose 1-2 fps on playback)."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:74
msgid "**Default Durations**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:76
msgid "Here you set the default duration of below items."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:80
msgid ":guilabel:`Bypass codec verification`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:81
msgid ":guilabel:`Use KDE job tracking for render jobs`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:86
msgid "Project Defaults"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:90
msgid ""
"Configures what the project settings will look like by default when you "
"choose File --> :ref:`New`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:100
msgid "Proxy Clips"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:104
msgid ""
"Configures what the proxy settings will be when you choose File --> :ref:"
"`New`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:110
msgid ""
":guilabel:`External proxy clips` When enabled it reads the proxy clips "
"generated by your video camera. More details see: :ref:"
"`using_camcorder_proxy_clips`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:115
msgid "Timeline"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:119
msgid "Configure how the timeline appears in **Kdenlive**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:127
msgid "**Thumbnails**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:129
msgid ":guilabel:`Enable for Video` Turns on video thumbnail by default."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:131
msgid ":guilabel:`Enable for Audio` Turns on audio thumbnail by default."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:133
msgid ""
":guilabel:`Separate audio channels` If checked you will get a separate "
"waveform in the audio thumbnail for each audio channel in the audio track. "
"If unchecked you will get a single waveform as the audio thumbnail."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:137
msgid ":guilabel:`Use FFmpeg for audio thumbnails (faster)`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:140
msgid "**Playback and Seeking**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:142
msgid ""
":guilabel:`Pause playback when seeking` **Enabled:** It stops playback while "
"you click on a new position in the timeline. **Disabled:** Playback is "
"ongoing while you click on a new position in the timeline. It allows looping "
"playback, see :ref:`loop_playback`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:144
msgid ""
":guilabel:`Jump to timeline start if playback is started on last frame in "
"timeline`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:146
msgid ":guilabel:`Seek to clip when adding effect`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:148
msgid "**Scrolling**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:150
msgid ":guilabel:`Autoscroll while playing`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:152
msgid ""
":guilabel:`Scroll vertically with scroll wheel, horizontally with Shift + "
"scroll wheel`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:154
msgid "**---------**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:156
msgid ":guilabel:`Display clip markers comments`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:158
msgid ""
":guilabel:`Default track height:` Defines the default track height in pixels "
"for the tracks on the timeline."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:160
msgid "**Raise properties pane when selecting in timeline**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:162
msgid ""
"More details :doc:`here </tips_and_tricks/tips_and_tricks/"
"automatically_raising_the_properties_pane>`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:165
msgid "**Multi stream audio clips**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:167
msgid ""
":guilabel:`On import, enable:` Select if :menuselection:`all audio "
"streams`, :menuselection:`first audio stream`, :menuselection:`first 2 audio "
"streams` should be imported."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:169
msgid ""
":guilabel:`Check if project contains enough audio tracks` If enabled "
"Kdenlive asks if it should generate the additional audio tracks needed "
"automatically."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:175
msgid "Tools"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:183
msgid ""
":guilabel:`Duplicate text` If selected, a 2-line subtitle gets split into "
"two subtitles by copying the content"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:184
msgid ""
":guilabel:`Split after first line` If selected, a 2-line subtitle gets split "
"into two subtitles by considering the line break"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:186
msgid "More details see :ref:`split_subtitle_after_first_line`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:192
msgid "Environment"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:196
msgid "MLT Environment"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:202
msgid "Environment variables on Windows"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:208
msgid ""
"Environment variables on Kdenlive normal installed on Linux (Appimage, "
"Flatpak, Snap may have integrated paths)"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:214
msgid "Environment variables on MacOS"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:217
msgid ""
"This setting tells **Kdenlive** where to find the MLT executables and "
"profile files. Only advanced users would really need to change these "
"settings. **Kdenlive** is basically a front end to the MLT program and this "
"setting tells **Kdenlive** where to find the engine that runs the whole "
"application."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:219
msgid ""
"Path to the MediaInfo file. If filled in Kdenlive shows more details in clip "
"properties."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:221
msgid "**Proxy and Transcode Jobs**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:223
msgid ""
":guilabel:`Concurrent threads` This will set the number of threads the "
"program will attempt to use when calling ffmpeg to encode :ref:`clips`. This "
"will be what kdenlive passes to the ffmpeg  *-threads* parameter. Increasing "
"this parameter may not have an effect if you have changed the proxy encoding "
"settings using :ref:`project_settings` to a codec that ffmpeg does not do "
"multi-thread on. (Multi-threading is supported for MPEG-2, MPEG-4, H.264, "
"and VP8)"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:227
msgid ":guilabel:`Use lower CPU priority for proxy and transcode tasks`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:229
msgid ""
"This adds a Kdenlive setting to lower the priority of the proxy rendering "
"(QProcess). This helps keep the main UI responsive when proxies are "
"rendering."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:233
msgid ":guilabel:`Cached Data`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:235
msgid ""
"Add a maximal cache size so that Kdenlive can check every 2 weeks if the "
"total cached data size exceeds this limit and warn the user."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:240
msgid ""
"**Processing and transcode jobs**: This is experimental and was removed in "
"ver 0.9.10. This number was passed to melts *real_time* consumer property. "
"This parameter increases the number of threads the program uses for video "
"decoding and processing (but not encoding which is controlled via :ref:"
"`render`).  See `mlt doco <https://www.mltframework.org/faq/#does-mlt-take-"
"advantage-of-multiple-cores-or-how-do-i-enable-parallel-processing>`_. Using "
"this has potential side effects - see `this <https://forum.kde.org/viewtopic."
"php%3Ff=265&t=122140.html#p317318>`_ forum post from the author of the Melt "
"program."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:242
msgid ""
"If you want to experiment with multi threading in versions higher than "
"0.9.10  you could add something like \"real_time=-4\" to a custom render "
"profile.  And yes that is a minus 4 in the example - as per the    `mlt doco "
"<https://www.mltframework.org/faq/#does-mlt-take-advantage-of-multiple-cores-"
"or-how-do-i-enable-parallel-processing>`_ - numbers <0 implement threading "
"without dropping frames."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:246
msgid "Default Folders"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:254
msgid "Default folders on Windows."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:260
msgid "Default folders on Linux."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:266
msgid "Default folders on MacOS."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:269
msgid ""
"This setting controls where **Kdenlive** expects project files to be by "
"default. It also controls what folder **Kdenlive** will use as a temporary "
"file storage location and it controls where files captured from an external "
"source will be saved."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:274
msgid "Default Apps"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:280
msgid ""
"This setting controls what external application opens when you choose :ref:"
"`edit_clip` for a clip in the project bin."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:282
msgid ""
":guilabel:`Image editing`: A free software would be `Krita <https://www."
"audacityteam.org/>`_."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:284
msgid ""
":guilabel:`Audio editing`: A free software would be `Audacity <https://krita."
"org/en/>`_."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:286
msgid ""
":guilabel:`Animation editing`: Kdenlive updates automatically files which "
"are saved in Glaxnimate. Glaxnimate can be downloaded from `here <https://"
"glaxnimate.mattbas.org/>`_ (Linux, Windows, Mac)."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:288
msgid ""
"**Mac user:** :ref:`Instruction <kdenlive_macos>` how to install and run "
"`dmg` files."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:290
msgid ""
"**Windows user:** Make sure all the paths points to an `.exe` file. "
"`Glaxnimate.exe` is in folder `C:/YourPath/glaxnimate-x86_64/glaxnimate/bin/"
"glaxnimate.exe`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:295
msgid ""
"There is no application for video editing - because **Kdenlive** is a video "
"editor."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:298
msgid "Mime types"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:304
msgid ""
"Specifies the Media Types (formerly known as MIME types) which Kdenlive can "
"working with."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:308
msgid "Added file type: `AVIF`, `HEIF` and `JPEG XL`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:310
msgid ""
"Added animation file type: `Json` (Lottie animations) and `rawr` (Glaxnimate "
"animation)"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:315
msgid "Colors and Guides"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:321
msgid ""
":guilabel:`Audio thumbnail colors` Click on the color bar and change the "
"color of the audio wave thumbnail."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:325
msgid ""
":guilabel:`Monitor overlay color` Click on the color bar and change the "
"color of the monitor overlay lines. See :ref:`ui-monitors_display_toolbar`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:327
msgid "Moved from tab :ref:`configure_playback`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:330
msgid ""
":guilabel:`Monitor background color` Click on the color bar and change the "
"color of the monitor background."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:334
msgid ""
":guilabel:`Guides and Markers Categories` This allows you to add categories. "
"Selected categories can be edited and deleted."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:340
msgid "Speech To Text"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:346
msgid ""
"More details about speech to text see :ref:`here <effects-speech_to_text>`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:352
msgid "Playback"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:356
msgid ""
"Configure the Video and Audio drivers and devices. For advanced users only."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:363
msgid "Playback view on Windows."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:369
msgid ":guilabel:`Audio driver` on Linux."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:375
msgid ":guilabel:`Audio driver` on MAcOS."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:377
msgid ":guilabel:`Audio driver` on Windows"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:379
msgid ""
"WinMM (Win7), Wasapi (Win10), DirectSound. If you have any audio issue or "
"playback stuttering you may change to another audio driver."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:381
msgid ""
"In version 0.9.4 of **Kdenlive**, checking the \"use Open GL for video "
"playback\" checkbox turns on the ability to have audio scrubbing available "
"for use in the clips.  Audio scrubbing lets you hear the audio at the "
"playhead position as you drag the playhead so you can quickly find a "
"particular sound or event in the audio. This feature can be useful for "
"placing the play head at the correct spot in the clip relative to an "
"important bit of audio."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:383
msgid ""
"In ver 15.04 or higher, there is no \"use Open GL for video playback\" "
"checkbox  - Open GL is used by default. On Windows you can set the OpenGL "
"backend under :menuselection:`Settings --> OpenGL Backend`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:387
msgid ":guilabel:`Monitor background color` moved to :ref:`configure_colors`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:389
msgid ""
":guilabel:`Enable Audio Scrubbing` and :guilabel:`Preview Volume` are "
"removed as the UI element is a duplicate of the volume slider in the "
"monitors hamburger menu. See :ref:`Clip Monitor<ui-"
"monitors_clip_monitor_hamburger>` and :ref:`Project "
"Monitor<project_monitor_hamburger>`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:396
msgid "Capture"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:400
msgid "Configure Screen Grab Capture"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:406
msgid ""
"These settings configure screen grab within **Kdenlive**. More details see :"
"ref:`here <capturing>`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:409
msgid "Blackmagic"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:415
msgid ""
"If you have a Blackmagic DecLink video capture card you can set here the "
"import parameter."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:420
msgid "Audio"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:426
msgid ""
"Microphone settings, either for screen :ref:`capturing` or for :ref:"
"`capturingaudio` direct into the timeline."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:430
msgid ":guilabel:`Disable countdown before recording`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:434
msgid ""
"At least Firewire capture was removed in porting to KDE 5 due to lack of "
"manpower."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:436
msgid "Following paragraph is for history reason only."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:438
msgid ""
"Configure the :ref:`capturing` devices (Firewire, FFmpeg, Screen Grab, "
"Blackmagic, Audio) from this section."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:442
msgid "Configure Firewire Capture"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:444
msgid ""
"The image shows the Configure Firewire capture tab which can be accessed "
"from the :menuselection:`Settings --> Configure Kdenlive` menu or from the "
"spanner icon in the :ref:`capturing`"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:453
msgid ""
"The firewire capture functionality uses the `dvgrab <http://linux.die.net/"
"man/1/dvgrab>`_ program. The settings applied here to define how dvgrab will "
"be used to capture the video."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:456
msgid "**Capture Format options** are"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:458
msgid "DV RAW"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:459
msgid "DV AVI Type 1"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:460
msgid "DV AVI Type 2"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:461
msgid "HDV"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:463
msgid ""
"The first three are quality-wise the same (exactly the same DV 25Mb/s "
"standard definition codec), just packed differently into the file. Type 2 "
"seems to be the most widely supported by other applications."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:465
msgid ""
"The raw format contains just the plain video frames (with audio interleaved) "
"without any additional information. Raw is useful for some Linux software. "
"Files in this format can also be played with Windows QuickTime when renamed "
"to :file:`file.dv`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:467
msgid ""
"AVI files may contain multiple streams. Typically, they include one video "
"and one audio stream. The native DV stream format already includes the audio "
"interleaved into its video stream. A type 1 DV AVI file only includes one DV "
"video stream where the audio must be extracted from the DV video stream. A "
"type 2 DV AVI file includes a separate audio stream in addition to the audio "
"data already interleaved in the DV video stream. Therefore, the type 2 DV "
"AVI file is redundant and consumes more space."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:469
msgid "HDV is a high-definition format used on tape-based HD camcorders."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:471
msgid ""
"**Add recording time to captured file name** option: If this is unchecked "
"then each captured file will get a sequential number post-pended to the file "
"names listed in the Capture file name setting. With this checked, date and "
"timestamp (derived from when the footage was captured) is post-pended to the "
"capture file name, e.g. **capture2012.07.15_11-38-37.dv**"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:473
msgid ""
"**Automatically start a new file on scene cut** option:  With this checked "
"it tries to detect whenever a new recording starts, and store it into a "
"separate file. This is the -autosplit parameter in  `dvgrab <http://linux."
"die.net/man/1/dvgrab>`_  and it works by detecting timecode discontinuities "
"from the source footage.  Where a timecode discontinuity is anything "
"backward or greater than one second it will start a new capture file."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:475
msgid ""
"The **dvgrab additional parameters** edit box allows you to add extra dvgrab "
"switches to the capture process that will run. See  `dvgrab manual <http://"
"linux.die.net/man/1/dvgrab>`_ for more info."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:481
msgid "Jog Shuttle"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:484
msgid ""
"Configure a connected Jog-Shuttle device. Contour ShuttlePro and Contour "
"ShuttleXpress are known to work."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:492
msgid "Linux"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:494
msgid ""
"Ensure that your Jog-Shuttle device is connected via USB and working. An "
"udev rule is necessary to correct the access rights to the device file: "
"Create a file /etc/udev/rules.d/90-contour-shuttleXpress.rules with the line:"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:505
msgid "for Contour ShuttleXpress or"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:516
msgid "for Contour ShuttlePRO V2. Obtain the device file by a command"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:523
msgid "The last line of the output says"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:530
msgid ""
"which should tell the device file to be entered into kdenlive's setting "
"dialog: In the text field enter /dev/input/**event3** (use the last word on "
"the line above to specify the device file in /dev/input), set the buttons "
"and apply the changes."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:539
msgid ""
"Enable Jog-Shuttle. For the Contour ShuttleXpress the buttons 5 - 9 are "
"relevant, whereas Contour ShuttlePro uses all buttons. The actions for the "
"jog- and the shuttle wheel are working as expected."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:542
msgid "Windows"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:544
msgid ""
"On the desktop, bottom-right opens the system tray. Right-click on the "
"Contour icon and choose \"Open control Panel\"."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:552
msgid ""
"In the configuration window choose under \"Application setting\" the program "
"\"Adobe Premiere Pro CS&amp;CC (Edit)\". Then click on :menuselection:"
"`Options --> Create new settings --> Copy contents from Current Settings`."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:560
msgid "Then choose Kdenlive.exe in C:\\Program Files\\kdenlive\\bin."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:568
msgid ""
"Now the basic functionality should work. Adjust the buttons of the shuttle "
"with shortcuts as you like."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:572
msgid ""
"You can make Kdenlive settings from scratch using :menuselection:`Options --"
"> Create new settings --> Create Empty Settings` when creating new settings."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:578
msgid "Transcode"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:581
msgid ""
"This controls the :ref:`transcode` functionality. The parameters section are "
"ffmpeg parameters. Find help on them by issuing ``ffmpeg -h`` at a command "
"line."
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:591
msgid "Transcode Options"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:596
msgid "Option"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:597
msgid "Description"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:598
msgid "Parameters"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:599
msgid "Meanings of Parameters"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:600
msgid "Wav 48000Hz"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:601
msgid "Extract audio as WAV file"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:602
msgid "-vn -ar 48000"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:603
msgid "-vn=disable video, -ar 48000 = set audio sampling rate to 48kHz"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:604
msgid "Remux with MKV"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:606
msgid "-vcodec copy -acodec copy -sn"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:607
msgid "copy the video and the audio. -sn = disable subtitles"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:608
msgid "Remux MPEG-2 PS/VOB"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:609
msgid "Fix audio sync in MPEG-2 vob files"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:610
msgid "-vcodec copy -acodec copy"
msgstr ""

#: ../../user_interface/menu/settings_menu/configure_kdenlive.rst:611
msgid "copy the video and the audio"
msgstr ""
