# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2021-12-20 14:12+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.0\n"

#: ../../effects_and_compositions/transitions/alphaout.rst:11
msgid "alphaout transition"
msgstr "overgang alphaout"

#: ../../effects_and_compositions/transitions/alphaout.rst:15
msgid ""
"This is the `Frei0r alphaout <https://www.mltframework.org/plugins/"
"TransitionFrei0r-alphaout/>`_ MLT transition."
msgstr ""
"Dit is de MLT-overgang `Frei0r alphaout <https://www.mltframework.org/"
"plugins/TransitionFrei0r-alphaout/>`_."

#: ../../effects_and_compositions/transitions/alphaout.rst:17
msgid "The alpha OUT operation."
msgstr "De alpha-OUT bewerking."

#: ../../effects_and_compositions/transitions/alphaout.rst:19
msgid "Yellow clip has a triangle alpha shape with min=0 and max=618."
msgstr "Gele clip heeft een driehoekige alfa-vorm met min=0 en max=618."

#: ../../effects_and_compositions/transitions/alphaout.rst:21
msgid "Green clip has rectangle alpha shape with min=0 and max=1000."
msgstr "Groene clip heeft rechthoekige alfa-vorm met min=0 en max=1000."

#: ../../effects_and_compositions/transitions/alphaout.rst:23
msgid "alphaout is the transition in between."
msgstr "alphaout is de overgang er tussen."

#~ msgid "Contents"
#~ msgstr "Inhoud"
