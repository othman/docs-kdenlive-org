# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2021-11-27 12:12+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/transitions/addition_alpha.rst:11
msgid "Addition_alpha transition"
msgstr "Overgang Alfa_toevoegen"

#: ../../effects_and_compositions/transitions/addition_alpha.rst:15
msgid ""
"This is the `Frei0r addition_alpha <https://www.mltframework.org/plugins/"
"TransitionFrei0r-addition_alpha/>`_ MLT transition."
msgstr ""
"Dit is de MLT-overgang `Frei0r addition_alpha <https://www.mltframework.org/"
"plugins/TransitionFrei0r-addition_alpha/>`_."

#: ../../effects_and_compositions/transitions/addition_alpha.rst:17
msgid "Perform an RGB[A] addition_alpha operation of the pixel sources."
msgstr "Voer een RGB[A] bewerking alfa_toevoeging uit op de pixelbronnen."

#~ msgid "Contents"
#~ msgstr "Inhoud"
