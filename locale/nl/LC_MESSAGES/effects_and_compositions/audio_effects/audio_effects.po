# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2021-12-04 11:21+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.08.3\n"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:16
msgid "Audio Effects"
msgstr "Geluidseffecten"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:19
msgid ""
"The following effects are available under the :menuselection:`Effects List --"
"> Audio`. The links below are to the MLT framework documentation for these "
"audio effects. Somewhat better documentation may be found at Steve Harris' "
"LADSPA Plugin Docs `here <http://plugin.org.uk/ladspa-swh/docs/ladspa-swh."
"html>`_."
msgstr ""
"De volgende effecten zijn beschikbaar onder de :menuselection:`Effectenlijst "
"--> Audio`. De onderstaande koppelingen zijn naar de documentatie van het "
"MLT-framework voor deze audio-effecten. Iets betere documentatie is te "
"vinden bij Steve Harris' LADSPA Plugin Docs `hier <http://plugin.org.uk/"
"ladspa-swh/docs/ladspa-swh.html>`_."

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:21
msgid ""
"`4 x 4 pole allpass <https://www.mltframework.org/plugins/FilterLadspa-1218/"
">`_"
msgstr ""
"`4 x 4 pole allpass <https://www.mltframework.org/plugins/FilterLadspa-1218/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:23
msgid "`Aliasing <https://www.mltframework.org/plugins/FilterLadspa-1407/>`_"
msgstr "`Aliasing <https://www.mltframework.org/plugins/FilterLadspa-1407/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:25
msgid ""
"`Allpass delay line cubic spline interpolation <https://www.mltframework.org/"
"plugins/FilterLadspa-1897/>`_"
msgstr ""
"`Allpass vertragingslijn kubische spline-interpolatie <https://www."
"mltframework.org/plugins/FilterLadspa-1897/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:27
msgid ""
"`Allpass delay line linear interpolation <https://www.mltframework.org/"
"plugins/FilterLadspa-1896/>`_"
msgstr ""
"`Allpass vertragingslijn lineaire interpolatie <https://www.mltframework.org/"
"plugins/FilterLadspa-1896/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:29
msgid ""
"`Allpass delay line noninterpolating <https://www.mltframework.org/plugins/"
"FilterLadspa-1895/>`_"
msgstr ""
"`Allpass vertragingslijn geen interpolatie <https://www.mltframework.org/"
"plugins/FilterLadspa-1895/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:31
msgid ""
"`AM pitchshifter <https://www.mltframework.org/plugins/FilterLadspa-1433/>`_"
msgstr ""
"`AM frequentieverschuiving <https://www.mltframework.org/plugins/"
"FilterLadspa-1433/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:33
msgid ""
"`Artificial latency <https://www.mltframework.org/plugins/FilterLadspa-1914/"
">`_"
msgstr ""
"`Kunstmatige vertraging <https://www.mltframework.org/plugins/"
"FilterLadspa-1914/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:35
msgid ""
"`Audio Divider (Suboctave Generator) <https://www.mltframework.org/plugins/"
"FilterLadspa-1186/>`_"
msgstr ""
"`Audio-opdeler (suboctaafgenerator) <https://www.mltframework.org/plugins/"
"FilterLadspa-1186/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:37
msgid ""
"`Audio Levels <https://www.mltframework.org/plugins/FilterAudiolevel/>`_"
msgstr ""
"`Audioniveaus <https://www.mltframework.org/plugins/FilterAudiolevel/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:39
msgid "`Audio Pan <https://www.mltframework.org/plugins/FilterPanner/>`_"
msgstr "`Audio-pan <https://www.mltframework.org/plugins/FilterPanner/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:41
msgid ""
"`Auto phaser <https://www.mltframework.org/plugins/FilterLadspa-1219/>`_"
msgstr ""
"`Auto phaser <https://www.mltframework.org/plugins/FilterLadspa-1219/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:43
msgid ""
"`Barry's Satan Maximiser <https://www.mltframework.org/plugins/"
"FilterLadspa-1408/>`_"
msgstr ""
"`Barry's Satan Maximiser <https://www.mltframework.org/plugins/"
"FilterLadspa-1408/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:45
msgid ""
"`Bode frequency shifter <https://www.mltframework.org/plugins/"
"FilterLadspa-1431/>`_"
msgstr ""
"`Bode frequentie verschuiven <https://www.mltframework.org/plugins/"
"FilterLadspa-1431/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:47
msgid ""
"`Bode frequency shifter (CV) <https://www.mltframework.org/plugins/"
"FilterLadspa-1432/>`_"
msgstr ""
"`Bode frequentie verschuiven (CV) <https://www.mltframework.org/plugins/"
"FilterLadspa-1432/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:49
msgid ""
"`Chebyshev distortion <https://www.mltframework.org/plugins/"
"FilterLadspa-1430/>`_"
msgstr ""
"`Chebyshev vervorming <https://www.mltframework.org/plugins/"
"FilterLadspa-1430/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:51
msgid ""
"`Comb delay line cubic spline interpolation <https://www.mltframework.org/"
"plugins/FilterLadspa-1888/>`_"
msgstr ""
"`Comb vertragingslijn kubische spline-interpolatie <https://www.mltframework."
"org/plugins/FilterLadspa-1888/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:53
msgid ""
"`Comb delay line linear interpolation <https://www.mltframework.org/plugins/"
"FilterLadspa-1887/>`_"
msgstr ""
"`Comb vertragingslijn lineaire interpolatie <https://www.mltframework.org/"
"plugins/FilterLadspa-1887/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:55
msgid ""
"`Comb delay line noninterpolating <https://www.mltframework.org/plugins/"
"FilterLadspa-1889/>`_"
msgstr ""
"`Comb vertragingslijn geen interpolatie <https://www.mltframework.org/"
"plugins/FilterLadspa-1889/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:57
msgid ""
"`Comb Filter <https://www.mltframework.org/plugins/FilterLadspa-1190/>`_"
msgstr ""
"`Comb-filter <https://www.mltframework.org/plugins/FilterLadspa-1190/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:59
msgid ""
"`Compand <https://www.mltframework.org/plugins/FilterAvfilter-compand/>`_"
msgstr ""
"`Compand <https://www.mltframework.org/plugins/FilterAvfilter-compand/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:61
msgid ""
"`Constant Signal Generator <https://www.mltframework.org/plugins/"
"FilterLadspa-1909/>`_"
msgstr ""
"`Constant signaalgenerator <https://www.mltframework.org/plugins/"
"FilterLadspa-1909/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:63
msgid "`Crossfade <https://www.mltframework.org/plugins/FilterLadspa-1915/>`_"
msgstr ""
"`Kruisvormig vagen <https://www.mltframework.org/plugins/FilterLadspa-1915/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:65
msgid ""
"`Crossfade (4 outs) <https://www.mltframework.org/plugins/FilterLadspa-1917/"
">`_"
msgstr ""
"`Kruisvormig vagen (4 uit) <https://www.mltframework.org/plugins/"
"FilterLadspa-1917/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:67
msgid ""
"`Crossover distortion <https://www.mltframework.org/plugins/"
"FilterLadspa-1404/>`_"
msgstr ""
"`Crossover vervorming <https://www.mltframework.org/plugins/"
"FilterLadspa-1404/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:69
msgid ""
"`DC Offset Remover <https://www.mltframework.org/plugins/FilterLadspa-1207/"
">`_"
msgstr ""
"`DC-offset verwijdering <https://www.mltframework.org/plugins/"
"FilterLadspa-1207/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:71
msgid "`Decimator <https://www.mltframework.org/plugins/FilterLadspa-1202/>`_"
msgstr "`Decimator <https://www.mltframework.org/plugins/FilterLadspa-1202/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:73
msgid "`Declipper <https://www.mltframework.org/plugins/FilterLadspa-1195/>`_"
msgstr "`Declipper <https://www.mltframework.org/plugins/FilterLadspa-1195/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:75
msgid "`Delayorama <https://www.mltframework.org/plugins/FilterLadspa-1402/>`_"
msgstr ""
"`Delayorama <https://www.mltframework.org/plugins/FilterLadspa-1402/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:77
msgid ""
"`Diode Processor <https://www.mltframework.org/plugins/FilterLadspa-1185/>`_"
msgstr ""
"`Diode-processor <https://www.mltframework.org/plugins/FilterLadspa-1185/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:79
msgid "`DJ EQ <https://www.mltframework.org/plugins/FilterLadspa-1901/>`_"
msgstr "`DJ EQ <https://www.mltframework.org/plugins/FilterLadspa-1901/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:81
msgid ""
"`DJ EQ (mono) <https://www.mltframework.org/plugins/FilterLadspa-1907/>`_"
msgstr ""
"`DJ EQ (mono) <https://www.mltframework.org/plugins/FilterLadspa-1907/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:83
msgid "`DJ flanger <https://www.mltframework.org/plugins/FilterLadspa-1438/>`_"
msgstr ""
"`DJ flanger <https://www.mltframework.org/plugins/FilterLadspa-1438/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:85
msgid ""
"`Dyson compressor <https://www.mltframework.org/plugins/FilterLadspa-1403/>`_"
msgstr ""
"`Dyson-compressor <https://www.mltframework.org/plugins/FilterLadspa-1403/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:87
msgid ""
"`Exponential signal decay <https://www.mltframework.org/plugins/"
"FilterLadspa-1886/>`_"
msgstr ""
"`Exponentiel signaalverval <https://www.mltframework.org/plugins/"
"FilterLadspa-1886/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:89
msgid ""
"`Fast overdrive <https://www.mltframework.org/plugins/FilterLadspa-1196/>`_"
msgstr ""
"`Snelle overdrive <https://www.mltframework.org/plugins/FilterLadspa-1196/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:91
msgid ""
"`Fast Lookahead limiter <https://www.mltframework.org/plugins/"
"FilterLadspa-1913/>`_"
msgstr ""
"`Snelle Lookahead limiter <https://www.mltframework.org/plugins/"
"FilterLadspa-1913/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:93
msgid "`Flanger <https://www.mltframework.org/plugins/FilterLadspa-1191/>`_"
msgstr "`Flanger <https://www.mltframework.org/plugins/FilterLadspa-1191/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:95
msgid ""
"`FM Oscillator <https://www.mltframework.org/plugins/FilterLadspa-1415/>`_"
msgstr ""
"`FM-oscillator <https://www.mltframework.org/plugins/FilterLadspa-1415/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:97
msgid ""
"`Foldover distortion <https://www.mltframework.org/plugins/FilterLadspa-1213/"
">`_"
msgstr ""
"`Foldover vervorming <https://www.mltframework.org/plugins/FilterLadspa-1213/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:99
msgid ""
"`Fractionally Addressed Delay Line <https://www.mltframework.org/plugins/"
"FilterLadspa-1192/>`_"
msgstr ""
"`Gedeeltelijk geadresseerde vertragingslijn <https://www.mltframework.org/"
"plugins/FilterLadspa-1192/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:101
msgid ""
"`Frequency tracker <https://www.mltframework.org/plugins/FilterLadspa-1418/"
">`_"
msgstr ""
"`Frequentievolger <https://www.mltframework.org/plugins/FilterLadspa-1418/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:103
msgid "`Gate <https://www.mltframework.org/plugins/FilterLadspa-1410/>`_"
msgstr "`Poort <https://www.mltframework.org/plugins/FilterLadspa-1410/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:105
msgid ""
"`Giant flange <https://www.mltframework.org/plugins/FilterLadspa-1437/>`_"
msgstr ""
"`Reusachtig flens <http://www.mltframework.org/bin/view/MLT/"
"FilterLadspa-1219>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:107
msgid ""
"`Glame Bandpass Analog Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1893/>`_"
msgstr ""
"`Glame analoog banddoorlaatfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1893/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:109
msgid ""
"`Glame Bandpass Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1892/>`_"
msgstr ""
"`Glame banddoorlaatfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1892/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:111
msgid ""
"`GLAME Butterworth Highpass <https://www.mltframework.org/plugins/"
"FilterLadspa-1904/>`_"
msgstr ""
"`GLAME Butterworth hoogdoorlaat <https://www.mltframework.org/plugins/"
"FilterLadspa-1904/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:113
msgid ""
"`GLAME Butterworth Lowpass <https://www.mltframework.org/plugins/"
"FilterLadspa-1903/>`_"
msgstr ""
"`GLAME Butterworth laagdoorlaat <https://www.mltframework.org/plugins/"
"FilterLadspa-1903/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:115
msgid ""
"`Glame Butterworth X-over Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1902/>`_"
msgstr ""
"`Glame Butterworth X-overfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1902/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:117
msgid ""
"`Glame Highpass Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1890/>`_"
msgstr ""
"`Glame hoogdoorlaatfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1890/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:119
msgid ""
"`Glame Lowpass Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1891/>`_"
msgstr ""
"`Glame laagdoorlaatfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1891/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:121
msgid ""
"`Gong beater <https://www.mltframework.org/plugins/FilterLadspa-1439/>`_"
msgstr ""
"`Gong beater <https://www.mltframework.org/plugins/FilterLadspa-1439/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:123
msgid "`Gong model <https://www.mltframework.org/plugins/FilterLadspa-1424/>`_"
msgstr ""
"`Gong model <https://www.mltframework.org/plugins/FilterLadspa-1424/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:125
msgid ""
"`GSM simulator <https://www.mltframework.org/plugins/FilterLadspa-1215/>`_"
msgstr ""
"`GSM simulator <https://www.mltframework.org/plugins/FilterLadspa-1215/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:127
msgid "`GVerb <https://www.mltframework.org/plugins/FilterLadspa-1216/>`_"
msgstr "`GVerb <https://www.mltframework.org/plugins/FilterLadspa-1216/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:129
msgid ""
"`Harmonic generator <https://www.mltframework.org/plugins/FilterLadspa-1220/"
">`_"
msgstr ""
"`Harmonische generator <https://www.mltframework.org/plugins/"
"FilterLadspa-1220/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:131
msgid ""
"`Hermes Filter <https://www.mltframework.org/plugins/FilterLadspa-1200/>`_"
msgstr ""
"`Hermes-filter <https://www.mltframework.org/plugins/FilterLadspa-1200/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:133
msgid ""
"`Higher Quality Pitch Scaler <https://www.mltframework.org/plugins/"
"FilterLadspa-1194/>`_"
msgstr ""
"`Hogere kwaliteit toonhoogte schalen <https://www.mltframework.org/plugins/"
"FilterLadspa-1194/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:135
msgid ""
"`Hilbert transformer <https://www.mltframework.org/plugins/FilterLadspa-1440/"
">`_"
msgstr ""
"`Hilbert-omvormer <https://www.mltframework.org/plugins/FilterLadspa-1440/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:137
msgid ""
"`Impulse convolver <https://www.mltframework.org/plugins/FilterLadspa-1199/"
">`_"
msgstr ""
"`Impulse convolver <https://www.mltframework.org/plugins/FilterLadspa-1199/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:139
msgid "`Inverter <https://www.mltframework.org/plugins/FilterLadspa-1429/>`_"
msgstr "`Inverter <https://www.mltframework.org/plugins/FilterLadspa-1429/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:141
msgid "`Karaoke <https://www.mltframework.org/plugins/FilterLadspa-1409/>`_"
msgstr "`Karaoke <https://www.mltframework.org/plugins/FilterLadspa-1409/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:143
msgid ""
"`L/C/R Delay <https://www.mltframework.org/plugins/FilterLadspa-1436/>`_"
msgstr ""
"`L/C/R vertraging <https://www.mltframework.org/plugins/FilterLadspa-1436/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:145
msgid "`LFO Phaser <https://www.mltframework.org/plugins/FilterLadspa-1217/>`_"
msgstr ""
"`LFO-phaser <https://www.mltframework.org/plugins/FilterLadspa-1217/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:147
msgid "`LS Filter <https://www.mltframework.org/plugins/FilterLadspa-1908/>`_"
msgstr "`LS-filter <https://www.mltframework.org/plugins/FilterLadspa-1908/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:149
msgid ""
"`Mag's Notch Filter <https://www.mltframework.org/plugins/FilterLadspa-1894/"
">`_"
msgstr ""
"`Mag's Notch-filter <https://www.mltframework.org/plugins/FilterLadspa-1894/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:151
msgid ""
"`Matrix Spatialiser <https://www.mltframework.org/plugins/FilterLadspa-1422/"
">`_"
msgstr ""
"`Matrix Spatialiser <https://www.mltframework.org/plugins/FilterLadspa-1422/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:153
msgid ""
"`Matrix: MS to Stereo <https://www.mltframework.org/plugins/"
"FilterLadspa-1421/>`_"
msgstr ""
"`Matrix: MS naar stereo <https://www.mltframework.org/plugins/"
"FilterLadspa-1421/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:155
msgid ""
"`Matrix: Stereo to MS <https://www.mltframework.org/plugins/"
"FilterLadspa-1420/>`_"
msgstr ""
"`Matrix: Stereo naar MS <https://www.mltframework.org/plugins/"
"FilterLadspa-1420/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:157
msgid ""
"`Modulatable delay <https://www.mltframework.org/plugins/FilterLadspa-1419/"
">`_"
msgstr ""
"`Moduleerbare vertraging <https://www.mltframework.org/plugins/"
"FilterLadspa-1419/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:159
msgid ""
"`Mono to stereo <https://www.mltframework.org/plugins/FilterLadspa-1406/>`_"
msgstr ""
"`Mono naar stereo <https://www.mltframework.org/plugins/FilterLadspa-1406/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:161
msgid ""
"`Multiband EQ <https://www.mltframework.org/plugins/FilterLadspa-1197/>`_"
msgstr ""
"`Multiband EQ <https://www.mltframework.org/plugins/FilterLadspa-1197/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:163
msgid ""
"`Multivoice Chorus <https://www.mltframework.org/plugins/FilterLadspa-1201/"
">`_"
msgstr ""
"`Multistem koor <https://www.mltframework.org/plugins/FilterLadspa-1201/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:165
msgid ""
"`Pitch Scaler <https://www.mltframework.org/plugins/FilterLadspa-1193/>`_"
msgstr ""
"`Pitch Scaler <https://www.mltframework.org/plugins/FilterLadspa-1193/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:167
msgid ""
"`Plate reverb <https://www.mltframework.org/plugins/FilterLadspa-1423/>`_"
msgstr "`Plaatgalm <https://www.mltframework.org/plugins/FilterLadspa-1423/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:169
msgid ""
"`Pointer cast distortion <https://www.mltframework.org/plugins/"
"FilterLadspa-1910/>`_"
msgstr ""
"`Aanwijzer cast vervorming <https://www.mltframework.org/plugins/"
"FilterLadspa-1910/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:171
msgid ""
"`Rate shifter <https://www.mltframework.org/plugins/FilterLadspa-1417/>`_"
msgstr ""
"`Frequentie verschuiven <https://www.mltframework.org/plugins/"
"FilterLadspa-1417/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:173
msgid ""
"`Retro Flanger <https://www.mltframework.org/plugins/FilterLadspa-1208/>`_"
msgstr ""
"`Retro Flanger <https://www.mltframework.org/plugins/FilterLadspa-1208/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:175
msgid ""
"`Reverse Delay (5s max) <https://www.mltframework.org/plugins/"
"FilterLadspa-1605/>`_"
msgstr ""
"`Omgekeerde vertraging (5s max) <https://www.mltframework.org/plugins/"
"FilterLadspa-1605/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:177
msgid ""
"`Ringmod with LFO <https://www.mltframework.org/plugins/FilterLadspa-1189/>`_"
msgstr ""
"`Ringmod met LFO <https://www.mltframework.org/plugins/FilterLadspa-1189/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:179
msgid ""
"`Ringmod with two inputs <https://www.mltframework.org/plugins/"
"FilterLadspa-1188/>`_"
msgstr ""
"`Ringmod met twee invoeren <https://www.mltframework.org/plugins/"
"FilterLadspa-1188/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:181
msgid "`SC1 <https://www.mltframework.org/plugins/FilterLadspa-1425/>`_"
msgstr "`SC1 <https://www.mltframework.org/plugins/FilterLadspa-1425/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:183
msgid "`SC2 <https://www.mltframework.org/plugins/FilterLadspa-1426/>`_"
msgstr "`SC2 <https://www.mltframework.org/plugins/FilterLadspa-1426/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:185
msgid "`SC3 <https://www.mltframework.org/plugins/FilterLadspa-1427/>`_"
msgstr "`SC3 <https://www.mltframework.org/plugins/FilterLadspa-1427/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:187
msgid "`SC4 <https://www.mltframework.org/plugins/FilterLadspa-1882/>`_"
msgstr "`SC4 <https://www.mltframework.org/plugins/FilterLadspa-1882/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:189
msgid "`SC4 mono <https://www.mltframework.org/plugins/FilterLadspa-1916/>`_"
msgstr "`SC4 mono <https://www.mltframework.org/plugins/FilterLadspa-1916/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:191
msgid "`SE4 <https://www.mltframework.org/plugins/FilterLadspa-1883/>`_"
msgstr "`SE4 <https://www.mltframework.org/plugins/FilterLadspa-1883/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:193
msgid ""
"`Signal sifter <https://www.mltframework.org/plugins/FilterLadspa-1210/>`_"
msgstr ""
"`Signaalzever <https://www.mltframework.org/plugins/FilterLadspa-1210/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:195
msgid ""
"`Simple amplifier <https://www.mltframework.org/plugins/FilterLadspa-1181/>`_"
msgstr ""
"`Eenvoudige versterker <https://www.mltframework.org/plugins/"
"FilterLadspa-1181/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:197
msgid ""
"`Simple Delay Line <https://www.mltframework.org/plugins/FilterLadspa-1043/"
">`_"
msgstr ""
"`Eenvoudige vertragingslijn <https://www.mltframework.org/plugins/"
"FilterLadspa-1043/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:199
msgid ""
"`Simple delay line cubic spline interpolation <https://www.mltframework.org/"
"plugins/FilterLadspa-1900/>`_"
msgstr ""
"`Eenvoudige vertragingslijn kubische spline-interpolatie <https://www."
"mltframework.org/plugins/FilterLadspa-1900/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:201
msgid ""
"`Simple delay line linear interpolation <https://www.mltframework.org/"
"plugins/FilterLadspa-1899/>`_"
msgstr ""
"`Eenvoudige vertragingslijn lineaire interpolatie <https://www.mltframework."
"org/plugins/FilterLadspa-1899/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:203
msgid ""
"`Simple Delay Line, noninterpolating <https://www.mltframework.org/plugins/"
"FilterLadspa-1898/>`_"
msgstr ""
"`Eenvoudige vertragingslijn, geen interpolatie <https://www.mltframework.org/"
"plugins/FilterLadspa-1898/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:205
msgid ""
"`Simple High Pass Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1042/>`_"
msgstr ""
"`Eenvoudige hoogdoorlaatfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1042/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:207
msgid ""
"`Simple Low Pass Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1041/>`_"
msgstr ""
"`Eenvoudige laagdoorlaatfilter <https://www.mltframework.org/plugins/"
"FilterLadspa-1041/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:209
msgid ""
"`Sine Oscillator (Freq:Audio,Amp:audio) <https://www.mltframework.org/"
"plugins/FilterLadspa-1044/>`_"
msgstr ""
"`Sinusoscillator (Freq:Audio,Amp:audio) <https://www.mltframework.org/"
"plugins/FilterLadspa-1044/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:211
msgid ""
"`Sine Oscillator (Freq:Audio,Amp:control) <https://www.mltframework.org/"
"plugins/FilterLadspa-1045/>`_"
msgstr ""
"`Sinusoscillator (Freq:Audio,Amp:control) <https://www.mltframework.org/"
"plugins/FilterLadspa-1045/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:213
msgid ""
"`Sine Oscillator (Freq:control,Amp:audio) <https://www.mltframework.org/"
"plugins/FilterLadspa-1046/>`_"
msgstr ""
"`Sinusoscillator (Freq:control,Amp:audio) <https://www.mltframework.org/"
"plugins/FilterLadspa-1046/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:215
msgid ""
"`Single band parametric <https://www.mltframework.org/plugins/"
"FilterLadspa-1203/>`_"
msgstr ""
"`Enkelband parametrisch <https://www.mltframework.org/plugins/"
"FilterLadspa-1203/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:217
msgid ""
"`Sinus wavewrapper <https://www.mltframework.org/plugins/FilterLadspa-1198/"
">`_"
msgstr ""
"`Sinusgolfomhuller <https://www.mltframework.org/plugins/FilterLadspa-1198/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:219
msgid ""
"`Smooth Decimator <https://www.mltframework.org/plugins/FilterLadspa-1414/>`_"
msgstr ""
"`Gladde decimator <https://www.mltframework.org/plugins/FilterLadspa-1414/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:221
msgid "`Sox band <https://www.mltframework.org/plugins/FilterSox-band/>`_"
msgstr "`Sox-band <https://www.mltframework.org/plugins/FilterSox-band/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:223
msgid "`Sox bass <https://www.mltframework.org/plugins/FilterSox-bass/>`_"
msgstr "`Sox-bas <https://www.mltframework.org/plugins/FilterSox-bass/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:225
msgid "`Sox echo <https://www.mltframework.org/plugins/FilterSox-echo/>`_"
msgstr "`Sox-echo <https://www.mltframework.org/plugins/FilterSox-echo/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:227
msgid ""
"`Sox flanger <https://www.mltframework.org/plugins/FilterSox-flanger/>`_"
msgstr "`Sox flens <https://www.mltframework.org/plugins/FilterSox-flanger/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:229
msgid "`Sox gain <https://www.mltframework.org/plugins/FilterSox-gain/>`_"
msgstr ""
"`Sox versterking <https://www.mltframework.org/plugins/FilterSox-gain/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:231
msgid "`Sox phaser <https://www.mltframework.org/plugins/FilterSox-phaser/>`_"
msgstr "`Sox phaser <https://www.mltframework.org/plugins/FilterSox-phaser/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:233
msgid ""
"`Sox stretch <https://www.mltframework.org/plugins/FilterSox-stretch/>`_"
msgstr ""
"`Sox stretch <https://www.mltframework.org/plugins/FilterSox-stretch/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:235
msgid ""
"`State Variable Filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1214/>`_"
msgstr ""
"`Statusvariabel filter <https://www.mltframework.org/plugins/"
"FilterLadspa-1214/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:237
msgid ""
"`Step Demuxer <https://www.mltframework.org/plugins/FilterLadspa-1212/>`_"
msgstr ""
"`Stap demuxer <https://www.mltframework.org/plugins/FilterLadspa-1212/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:239
msgid ""
"`Surround matrix encoder <https://www.mltframework.org/plugins/"
"FilterLadspa-1401/>`_"
msgstr ""
"`Surround matrix codeerder <https://www.mltframework.org/plugins/"
"FilterLadspa-1401/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:241
msgid ""
"`Tape Delay Simulation <https://www.mltframework.org/plugins/"
"FilterLadspa-1211/>`_"
msgstr ""
"`Tapevetragingssimulation <https://www.mltframework.org/plugins/"
"FilterLadspa-1211/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:243
msgid ""
"`Transient mangler <https://www.mltframework.org/plugins/FilterLadspa-1206/"
">`_"
msgstr ""
"`Transient mangler <https://www.mltframework.org/plugins/FilterLadspa-1206/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:245
msgid ""
"`Triple band parametric with shelves <https://www.mltframework.org/plugins/"
"FilterLadspa-1204/>`_"
msgstr ""
"`Driebandig parametrisch met opleggingen <https://www.mltframework.org/"
"plugins/FilterLadspa-1204/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:247
msgid ""
"`Valve rectifier <https://www.mltframework.org/plugins/FilterLadspa-1405/>`_"
msgstr ""
"`Ventielgelijkrichter <https://www.mltframework.org/plugins/"
"FilterLadspa-1405/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:249
msgid ""
"`Valve saturation <https://www.mltframework.org/plugins/FilterLadspa-1209/>`_"
msgstr ""
"`Ventielverzadiging <https://www.mltframework.org/plugins/FilterLadspa-1209/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:251
msgid ""
"`VyNil (Vinyl Effect) <https://www.mltframework.org/plugins/"
"FilterLadspa-1905/>`_"
msgstr ""
"`VyNil (Vinyleffect) <https://www.mltframework.org/plugins/FilterLadspa-1905/"
">`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:253
msgid ""
"`Wave shaper <https://www.mltframework.org/plugins/FilterLadspa-1187/>`_"
msgstr ""
"`Golfvormer <https://www.mltframework.org/plugins/FilterLadspa-1187/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:255
msgid ""
"`Wave Terrain Oscillator <https://www.mltframework.org/plugins/"
"FilterLadspa-1412/>`_"
msgstr ""
"`Golfterreinoscillator <https://www.mltframework.org/plugins/"
"FilterLadspa-1412/>`_"

#: ../../effects_and_compositions/audio_effects/audio_effects.rst:257
msgid "`z-1 <https://www.mltframework.org/plugins/FilterLadspa-1428/>`_"
msgstr "`z-1 <https://www.mltframework.org/plugins/FilterLadspa-1428/>`_"

#~ msgid "Contents"
#~ msgstr "Inhoud"

#~ msgid "Audio Levels"
#~ msgstr "Geluidsniveaus"

#~ msgid "Mono to stereo"
#~ msgstr "Mono naar stereo"
