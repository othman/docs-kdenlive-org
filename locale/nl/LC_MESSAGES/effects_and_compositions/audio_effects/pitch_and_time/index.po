# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2023 Freek de Kruijf <freekdekruijf@kde.nl>
# Freek de Kruijf <f.de.kruijf@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-15 00:36+0000\n"
"PO-Revision-Date: 2023-11-26 19:00+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:1
msgid "Kdenlive Audio Effects - Pitch and Time"
msgstr "Kdenlive audio-effecten - toonhoogte en filters"

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, audio "
"effects, pitch, time"
msgstr ""
"KDE, Kdenlive, documentatie, gebruikershandleiding, videobewerker, open-"
"source, audio-effecten, frequentie, tijd"

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:24
msgid "Pitch and Time"
msgstr "Frequentie en tijd"

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:26
msgid ""
"This category consists of effects and filters for things like octave shift "
"and pitch adjustment."
msgstr ""

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:38
msgid ""
"The following filters and effects are available but not documented here. "
"Please refer to the |avfilter| documentation for more details."
msgstr ""

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:40
msgid "rubberband_octave_shift"
msgstr ""

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:41
msgid "rubberband_pitch_scale"
msgstr ""

#: ../../effects_and_compositions/audio_effects/pitch_and_time/index.rst:42
msgid "sox_stretch"
msgstr ""
