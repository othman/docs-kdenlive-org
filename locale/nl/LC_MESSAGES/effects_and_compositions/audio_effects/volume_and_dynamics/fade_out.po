# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2023 Freek de Kruijf <freekdekruijf@kde.nl>
# Freek de Kruijf <f.de.kruijf@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-15 00:36+0000\n"
"PO-Revision-Date: 2023-11-18 13:57+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.3\n"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:0
msgid "Status"
msgstr "Status"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:0
msgid "Keyframes"
msgstr "Keyframes"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:0
msgid "Source library"
msgstr "Bronnenbibliotheek"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:0
msgid "Available"
msgstr "Beschikbaar"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:0
msgid "On Master only"
msgstr "Alleen op master"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:0
msgid "Known bugs"
msgstr "Bekende bugs"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:1
msgid "Kdenlive Audio Effects - Fade Out"
msgstr "Kdenlive audio-effecten - uitvagen"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:1
msgid ""
"KDE, Kdenlive, documentation, user manual, video editor, open source, audio "
"effects, volume, dynamics, fade out"
msgstr ""

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:15
msgid "Fade Out"
msgstr "Uitvagen"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:25
msgid "Maintained"
msgstr ""

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:27
#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:33
#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:35
msgid "No"
msgstr "Nee"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:29
msgid "avfilter"
msgstr ""

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:31
msgid "|linux| |appimage| |windows| |apple|"
msgstr ""

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:42
#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:55
msgid "Description"
msgstr "Beschrijving"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:43
msgid ""
"Fades out the audio track. Instead of keyframes the effect uses duration (hh:"
"mm:ss:ff). In addition to using the slider the duration can be entered "
"manually, or in the timeline by dragging the red handle\\ [1]_."
msgstr ""

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:47
msgid "Parameters"
msgstr "Parameters"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:53
msgid "Parameter"
msgstr "Parameter"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:54
msgid "Value"
msgstr "Waarde"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:57
msgid "**Duration**"
msgstr "**Tijdsduur**"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:58
msgid "Time"
msgstr "Tijd"

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:59
msgid ""
"Defines the duration for the fade out starting before the end of the clip."
msgstr ""

#: ../../effects_and_compositions/audio_effects/volume_and_dynamics/fade_out.rst:63
msgid ""
"The handle is always available in the timeline clip. In fact, dragging the "
"handle automatically adds the fade out effect to the clip's effect stack if "
"it is not there already."
msgstr ""
