# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
#
# SPDX-FileCopyrightText: 2023 Freek de Kruijf <freekdekruijf@kde.nl>
# Ronald Stroethoff <stroet43@zonnet.nl>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-12-09 00:36+0000\n"
"PO-Revision-Date: 2023-12-12 14:16+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**Status**"
msgstr "**Status**"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**Keyframes**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**Source library**"
msgstr "**Bronnenbibliotheek**"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**Source filter**"
msgstr "**Filter op de bron**"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**Available**"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**On Master only**"
msgstr "**Alleen op master**"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:0
msgid "**Known bugs**"
msgstr "**Bekende bugs**"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:1
msgid "Kdenlive Video Effects - Mask Apply"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:1
msgid ""
"KDE, Kdenlive, video editor, help, learn, easy, effects, filter, video "
"effects, alpha, chroma key, keying, mask apply"
msgstr ""

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:18
msgid "Mask Apply"
msgstr "Masker toepassen"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:29
msgid "Maintained"
msgstr "Onderhouden"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:31
#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:39
#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:41
msgid "No"
msgstr "Nee"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:33
msgid "MLT"
msgstr "MLT"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:35
msgid "mask_apply"
msgstr "mask_apply"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:37
msgid "|linux| |appimage| |windows| |apple|"
msgstr "|linux| |appimage| |windows| |apple|"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:47
msgid "Description"
msgstr "Beschrijving"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:48
msgid ""
"Apply the previous effects starting with an effect with \"(Mask)\" in its "
"name."
msgstr ""
"Pas de vorige effecten toe die starten met een effect met \"(Masker)\" in "
"zijn naam."

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:52
msgid "Notes"
msgstr "Notities"

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:53
msgid ""
"This is a very powerful effect function as it does not do anything itself "
"but instruct Kdenlive to apply the effects beginning with the (Mask) effect "
"only to the area defined by that (Mask) effect."
msgstr ""
"Dit is een erg handig effect omdat het niets zelf doet maar Kdenlive "
"instrueert om een van de effecten die met (Mask) beginnen toe te passen op "
"een vlak dat door het (Mask) effect wordt gedefinieerd."

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:55
msgid ""
"See the :ref:`Masking Effects <effects-masking_effects>` section for more "
"details."
msgstr ""
"Zie de sectie over :ref:`Masker Effecten <effects-masking_effects>` voor "
"meer details."

#: ../../effects_and_compositions/video_effects/alpha_mask_keying/mask_apply.rst:57
msgid ""
"In the *Template* effects category there is a pre-setup *Secondary Color "
"Correction* that uses a (Mask) effect and the Mask Apply effect."
msgstr ""
"In de categorie *Sjabloon* effecten is er een vooringestelde *Secundaire "
"kleurcorrectie* dat een (Masker) effect en het Masker toepassen effect "
"gebruikt."

#~ msgid "Secondary Color Correction Area Selection (Mask) effect panel"
#~ msgstr "Paneel voor Secundaire kleurcorrectiegebiedsselectie (masker)"
