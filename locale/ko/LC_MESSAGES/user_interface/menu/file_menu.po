# Copyright (C) licensed under the  <a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons License SA 4.0</a> unless stated otherwise
# This file is distributed under the same license as the Kdenlive Manual package.
# Shinjo Park <kde@peremen.name>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: Kdenlive Manual \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-11-05 12:20+0000\n"
"PO-Revision-Date: 2022-05-08 16:40+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: ../../user_interface/menu/file_menu.rst:1
msgid "File menu in Kdenlive video editor"
msgstr ""

#: ../../user_interface/menu/file_menu.rst:1
msgid ""
"KDE, Kdenlive, file, menu, new, open, save, revert, transcode, clips, quit, "
"documentation, user manual, video editor, open source, free, learn, easy"
msgstr ""

#: ../../user_interface/menu/file_menu.rst:31
msgid "File Menu"
msgstr "파일 메뉴"

#: ../../user_interface/menu/file_menu.rst:33
msgid ""
"The File Menu contains the normal file operations as well as the option to "
"import and export project files using the OpenTimelineIO format."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:38
msgid "New"
msgstr "새로 만들기"

#: ../../user_interface/menu/file_menu.rst:40
msgid ""
"Creates a new Kdenlive project. The default keyboard shortcut is :kbd:`Ctrl"
"+N`."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:42
msgid "See :ref:`quickstart`."
msgstr ":ref:`quickstart` 문서도 참조하십시오."

#: ../../user_interface/menu/file_menu.rst:44
msgid ""
"The default settings that appear on this feature are defined under :"
"menuselection:`Settings --> Configure Kdenlive`. See the :ref:"
"`configure_kdenlive` section of this documentation for more details."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:47
msgid "Open..."
msgstr "열기..."

#: ../../user_interface/menu/file_menu.rst:49
msgid ""
"Opens a project that has been saved in the :ref:`project <project>` format "
"file. The default keyboard shortcut is :kbd:`Ctrl+O`."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:52
msgid "Open Recent"
msgstr "최근 항목 열기"

#: ../../user_interface/menu/file_menu.rst:54
msgid ""
"Displays a picklist of recently saved files (up to 10) to choose from. Click "
"the :menuselection:`Clear List` choice when you want to start over with a "
"fresh list."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:57
msgid "Save"
msgstr "저장"

#: ../../user_interface/menu/file_menu.rst:59
msgid ""
"Saves the current state of the project in the :ref:`project <project>` "
"format file. Prompts for a file name if this is the first time the file is "
"being saved. The default keyboard shortcut is :kbd:`Ctrl+S`."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:62
msgid "Save As..."
msgstr "다른 이름으로 저장..."

#: ../../user_interface/menu/file_menu.rst:64
msgid ""
"Saves the current state of the project in the :ref:`project <project>` "
"format file. Prompts for a file name. Kdenlive then continues with the new "
"project. The default keyboard shortcut is :kbd:`Ctrl+Shift+S`."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:67
msgid "Save Copy..."
msgstr "복사본 저장..."

#: ../../user_interface/menu/file_menu.rst:69
msgid ""
"Saves the current state of the project in the :ref:`project <project>` "
"format file as a copy. Prompts for a file name. Kdenlive returns to the "
"current project."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:72
msgid "Revert"
msgstr "되돌리기"

#: ../../user_interface/menu/file_menu.rst:74
msgid ""
"This abandons any changes to the project you have made since last saving and "
"reverts back to the last saved version of the project."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:77
msgid "Transcode Clips..."
msgstr "클립 트랜스코드..."

#: ../../user_interface/menu/file_menu.rst:86
#, fuzzy
#| msgid "Transcode Clips..."
msgid "Transcode clips"
msgstr "클립 트랜스코드..."

#: ../../user_interface/menu/file_menu.rst:89
msgid ""
"Use this to convert a video or audio clip from one codec/format to another."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:91
msgid ""
"Choose one source file or multiple source files and a profile that "
"represents the desired destination codec/format. Optionally change the "
"destination path and file name and hit :guilabel:`Start`. Otherwise, hit :"
"guilabel:`Abort` to close the windows."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:93
msgid ""
"Transcoding a clip should be faster than loading the clip into the timeline "
"and re-encoding it into a different format."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:95
msgid ""
":guilabel:`Add clip to project` controls if after the conversion, the new "
"clip is added to the :ref:`Project Bin <project_tree>`."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:97
msgid ""
":guilabel:`Close after encode` Uncheck this checkbox if there is the need to "
"convert to another format after the conversion."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:111
msgid "Quit"
msgstr "끝내기"

#: ../../user_interface/menu/file_menu.rst:113
msgid "Exits **Kdenlive**."
msgstr ""

#: ../../user_interface/menu/file_menu.rst:115
msgid ""
"Prompts you to save any unsaved changes. The default keyboard shortcut is :"
"kbd:`Ctrl+Q`."
msgstr ""

#~ msgid "Close"
#~ msgstr "닫기"

#~ msgid "Contents"
#~ msgstr "목차"

#~ msgid "Contents:"
#~ msgstr "목차:"
